#!/bin/bash
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
#move files
echo_hello() {
  echo "Hello world"
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
{
  #set -e
  logit INFO 'Starting Exemplar Install'
  echo_hello
  rc=$?
  rc=1
  if [ $rc == "0" ]; then
	 logit INFO "Exemplar install successful"
  else
     echo expression evaluated as false
  fi
}