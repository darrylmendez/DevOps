#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

# install nginx
apt-get update
apt-get -y install nginx

# make sure nginx is started
service nginx start

echo $0
echo $1
echo $2

logger -s "Logging $0 " $0
logger -s "Logging $1 " $1
logger -s "Logging $2 " $2