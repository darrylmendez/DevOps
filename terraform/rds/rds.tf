resource "aws_db_subnet_group" "oracledb-subnet" {
    name = "oracledb-subnet"
    description = "RDS subnet group"
    subnet_ids = ["${aws_subnet.main-private-1.id}","${aws_subnet.main-private-2.id}"]
}

resource "aws_db_parameter_group" "oracledb-parameters" {
    name = "oracle-db-parameters"
    family = "oracle-ee-12.1"
    description = "OracleDB parameter group"
}


resource "aws_db_instance" "oracledb" {
  allocated_storage    = 100    # 100 GB of storage, gives us more IOPS than a lower number
  engine               = "oracle-ee"
  engine_version       = "12.1.0.2.v8"
  iops				   = 1000 #Provisioned IOPS
  instance_class       = "db.t2.micro"    # use micro if you want to use the free tier
  identifier           = "rlimsdb" #Customize to RLIMS
  name                 = "RLIMSDB"
  username             = "root"   # username RLIMSADMIN
  password             = "${var.RDS_PASSWORD}" # password AbbVie2013
  db_subnet_group_name = "${aws_db_subnet_group.oracledb-subnet.name}"
  parameter_group_name = "${aws_db_parameter_group.oracledb-parameters.name}"
  multi_az             = "false"     # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids = ["${aws_security_group.allow-oracledb.id}"]
  storage_type         = "gp2"
  backup_retention_period = 30    # how long you’re going to keep your backups
  availability_zone = "${aws_subnet.main-private-1.availability_zone}"   # prefered AZ
  tags {
      Name = "oracledb-instance"
  }
}
