#!/bin/bash

#AbbVie RedHat Enterprise Linux (RHEL) Base AMI Creation Script
#Author: Justin Doss (dossjj) justin.doss@abbvie.com
#Update: Thomas Baik (baikts) thomas.baik@abbvie.com
#Date: 01/22/2018

# Move Java to /opt/directory
echo 'Moving Java'
sudo mv /home/ec2-user/jdk-8u162-linux-x64.rpm /opt/

#Install and configure Java
echo 'Installing Java.....'
echo 'cd /opt/'
cd /opt/

#echo 'sudo wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u162-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u151-linux-x64.tar.gz"'
#sudo wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u162-b12/e758a0de34e24606bca991d704f6dcbf/jdk-8u162-linux-x64.tar.gz"
#sudo curl -v -j -k -L -H "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u162-b12/jdk-8u162-linux-x64.rpm

sudo rpm -ivh jdk-8u162-linux-x64.rpm
#echo 'sudo tar xvzf jdk-8u162-linux-x64.tar.gz'
#sudo tar xvzf jdk-8u162-linux-x64.tar.gz
#echo 'cd jdk1.8.0_162/'
#cd jdk1.8.0_162/

#echo 'export JAVA_HOME=/opt/jdk1.8.0_162/bin' >> /home/ec2-user/.bash_profile
#echo 'export JRE_HOME=/opt/jdk1.8.0_162/jre/bin' >> /home/ec2-user/.bash_profile
#echo 'export PATH=$JAVA_HOME:$JRE_HOME:$PATH' >> /home/ec2-user/.bash_profile
#source /home/ec2-user/.bash_profile

#echo 'sudo alternatives --install /usr/bin/java java /opt/jdk1.8.0_162/bin/java 2'
#sudo alternatives --install /usr/bin/java java /opt/jdk1.8.0_162/bin/java 2
#echo 'sudo alternatives --config java'
#sudo alternatives --config java

#echo 'sudo alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_162/bin/jar 2'
#sudo alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_162/bin/jar 2
#echo 'sudo alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_162/bin/javac 2'
#sudo alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_162/bin/javac 2
#echo 'sudo alternatives --set jar /opt/jdk1.8.0_151/bin/jar'
#sudo alternatives --set jar /opt/jdk1.8.0_162/bin/jar
#echo 'sudo alternatives --set javac /opt/jdk1.8.0_162/bin/javac'
#sudo alternatives --set javac /opt/jdk1.8.0_162/bin/javac
#echo 'Jave install complete'

echo 'Confirm Java version'
java -version

echo '.....Java installation and configuration complete'
