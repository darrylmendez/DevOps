#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

#BLS Updates 
echo 'Performing update.....'
echo 'sudo yum update -y'
sudo yum update -y
echo '.....Update complete'

#Perform Upgrade
echo 'Performing upgrade.....'
echo 'sudo yum upgrade -y'
sudo yum upgrade -y
echo '.....Upgrade complete'

#Install vim
echo 'Installing vim text editor.....'
echo 'sudo yum install vim -y'
sudo yum install vim -y
echo '.....vim installation complete'

#Install wget
echo 'Installing wget.....'
echo 'sudo yum install wget -y'
sudo yum install wget -y
echo '.....wget installation complete'

#Install zip and unzip
echo 'Installing unzip and zip.....'
echo 'sudo yum install -y unzip zip'
sudo yum install -y unzip zip
echo '.....unzip and zip installation complete'

echo 'Installing libaio.....'
echo 'sudo yum -y install libaio'

sudo yum -y install libaio

echo '.....Base installation and configuration complete'

echo 'chmod +x /tmp/rhel-java.sh'
chmod +x /tmp/rhel-java.sh

echo 'sudo /tmp/rhel-java.sh' 
sudo /tmp/rhel-java.sh
		  
