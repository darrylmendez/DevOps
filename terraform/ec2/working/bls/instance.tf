resource "aws_instance" "rlims-instance" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  provisioner "file" {
	source = "/home/ubuntu/DownLoads/jdk-8u162-linux-x64.rpm"
	destination="/home/ec2-user/jdk-8u162-linux-x64.rpm"
  }
  
  provisioner "file" {
	source = "script.sh"
	destination="/tmp/script.sh"
  }
  provisioner "file" {
	source = "rhel-java.sh"
	destination="/tmp/rhel-java.sh"
  }
      
  provisioner "remote-exec" {
		inline = [
			"chmod +x /tmp/script.sh",
             "sudo /tmp/script.sh"
			 	
			]
		}
	connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}
