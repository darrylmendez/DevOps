provider "aws" { 
    region = "${var.AWS_REGION}"
}
provider "aws" {
	alias  = "useast"
	region = "us-east-1"
}
resource "aws_s3_bucket" "bucket" {
	provider = "aws.useast" # Set this and remove the region argument
	bucket   = "abbvie-rlims"
}