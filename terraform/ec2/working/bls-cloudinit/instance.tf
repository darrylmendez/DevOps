resource "aws_instance" "rlims-bls-instance" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"
  

  # the VPC subnet
  # move to private id 
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-bls.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads"
    destination = "/tmp/DownLoads"
  }

  # provisioner "remote-exec" {
  #		inline = [
  #			"chmod +x /tmp/scripts/*.sh",
  #            "sudo /tmp/scripts/utils.sh",
  #            "sudo /tmp/rhel-java.sh",
  #            "sudo /tmp/oracle-client.sh ${aws_db_instance.oracledb.endpoint}",
  #			]
  #		}
  #	connection {
  #    user = "${var.INSTANCE_USERNAME}"
  #    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  #  }
  tags = {
    Name = "${var.customer}-${var.environment}-BLS-Server"
    environment = "${var.environment}"
  }
  
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/*.sh",
      "sudo /tmp/scripts/utils.sh",
      "sudo /tmp/bls.sh ${var.guid}",
    ]
  }
  connection {
    user        = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}

resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "us-east-2a"
  size              = 1
  type              = "gp2"

  tags {
    Name = "extra volume data"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-bls-attachment" {
  device_name = "${var.INSTANCE_DEVICE_NAME}"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.rlims-bls-instance.id}"
}

resource "aws_instance" "rlims-ui-instance" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-ui.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads"
    destination = "/tmp/"
  }
#push tomcat to packer
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/*.sh",
      "sudo /tmp/scripts/tomcat.sh", 
	  "sudo /tmp/scripts/ui.sh" 
    ]
  }

  connection {
    user        = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
  
  tags = {
    Name = "${var.customer}-${var.environment}-UI-Server"
    environment = "${var.environment}"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-ui-attachment" {
  device_name = "${var.INSTANCE_DEVICE_NAME}"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.rlims-ui-instance.id}"
}
