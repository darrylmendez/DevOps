#!/bin/bash

#RLIMS UI Server Installation Script
#Author: Darryl Mendez (mendedx) darryl.mendez@abbvie.com
#Date: 04/02/2018

#Define Variables
EXEMPLAR_BASE=exemplarlims_v961_b664
EXEMPLAR_BASE_LINUX_EXEC=exemplarlims_v961_b664
EXEMPLAR_BASE_EXTRACT_DIR=/tmp/exemplar_base_extract
TOMCAT_DIR=/opt/tomcat
APP_GUID=$1

#Create group 
#moved to cloudinit
create_group() {
  if ! grep -q "$1" /etc/group; then
    sudo groupadd "$1"
  fi
}
#Create User
#moved to cloudinit
create_user() {
  if ! id "$1" > /dev/null 2>&1; then
    sudo useradd "$1" -g "$1"
  fi
}
#unzip files
unzip_file() {
  sudo unzip "$1" -d "$2"
}
#Update OS Packages
update_os() {
  sudo yum -y update
}

#0 - 13
extract_exemplar() {
#Extract velox_portal.war and veloxClient.war files
 unzip_file /tmp/"$EXEMPLAR_BASE".zip $EXEMPLAR_BASE_EXTRACT_DIR 
		|| logit ERROR "Unzip File $EXEMPLAR_BASE to $EXEMPLAR_BASE_EXTRACT_DIR Failed"
}
#1 - 21
#copy_Exemplar_Files from Amazon S3 (BLS Server)
#Copy velox_portal.war and veloxClient.war files from BLS server to /opt/tomcat/webapps directory on UI Server. 
#Change directory to where these war files are present. 
#Path on BLS Server - /usr/local/<Exemplar_Install_Directory>/client
#Path on UI Server - /opt/tomcat/webapps
#$ cd /home/ec2-user
#$ sudo cp *.war /opt/tomcat/webapps
copy_Exemplar_Files() {
  cp $EXEMPLAR_BASE_EXTRACT_DIR/velox_portal/velox_portal.war $TOMCAT_DIR/webapps
  cp $EXEMPLAR_BASE_EXTRACT_DIR/veloxClient/veloxClient.war $TOMCAT_DIR/webapps 
}

#3 - 42
#Extract tomcat plugins
extract_tc_plugins() {
  sudo java -Dappguid=$APP_GUID $TOMCAT_DIR/webapps
}


#2 - Verify war files installed in Tomcat
verify_war_tc(){
}
#3
#Copy the .keystore file from BLS server to UI Server
#Path in BLS Server - /usr/local/<Exemplar_Install_Directory>/ssl-certs
#Path in UI Server - /opt/tomcat/webapps/velox_portal/WEB-INF/classes & /opt/tomcat/webapps/velox_portal/WEB-INF/classes
#Copy the .keystore file from BLS server to UI Server
#Switch to root user:
#$ sudo su -
#$ cd /home/ec2-user
#$ cp .keystore /opt/tomcat/webapps/velox_portal/WEB-INF/classes
#$ mv .keystore /opt/tomcat/webapps/velox_portal/WEB-INF/classes

#Logout of root user
#$ exit
copy_key_store() {
}
#4 - Configure PortalClient.Properties
#Login as root user
#$ sudo su –
#$ cd /opt/tomcat/webapps/velox_portal/config
#$ vim PortalClient.Properties

#Properties file must match below:
#trusted.client.keystore=.keystore
#trusted.client.keystore.password=AbbVie2013
#trusted.client.key.password=AbbVie2013
#exemplarlims.server.ip=<HOSTNAME OF BLS>
#exemplarlims.server.name=DataMgmtServer
#email.host=15.163.124.148
#email.smtpport=25
#email.sender=rlims.support@abbvie.com
configure_portal_client() {
}
#5 - Configure Exemplar Web Client Properties
#cd /opt/tomcat/webapps/veloxClient/config
#sudo vim ExemplarWebClient.properties

#exemplarlims.server.ip=<HOSTNAME OF BLS>
#exemplarlims.server.name= DataMgmtServer
#trusted.client.key.password=AbbVie2013
#trusted.client.keystore=.keystore
#trusted.client.keystore.password=AbbVie2013
#email.host=15.163.124.148
#email.smtpport=25
#email.sender=rlims.support@abbvie.com

#6 - Configure server.xml for SSL
#$ cd /opt/tomcat/conf
#$ vim server.xml

#Add the following to the server.xml file:

#<Connector port="8443" protocol="org.apache.coyote.http11.Http11Nio2Protocol"
#maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
#compression="on" compressionMinSize="2048"
#compressableMimeType="text/html,text/xml,text/javascript,application/x-javascript,application/javascript,text/css"
#useSendfile="false">
#<UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol" />
#<SSLHostConfig>
#<Certificate certificateKeystoreFile="/opt/tomcat/webapps/velox_portal/WEB-INF/classes/.keystore"
#certificateKeystorePassword="AbbVie2013"
#certificateKeyAlias="tomcat"
#/>
#</SSLHostConfig>
#</Connector>
configure_server_xml() {
}
#7 - Shutdown tomcat server and start as tomcat user
#Exit out of root user:
# exit

#$ cd /opt/tomcat/bin
#$ sudo ./shutdown.sh 

#$ sudo /bin/su -p -s /bin/sh tomcat /opt/tomcat/bin/startup.sh
start_tc(){
}
{
  set -e
	
  logit INFO 'Starting Exemplar Extract'
  extract_exemplar 
  ret_code = $?
  if [$ret_code -eq "0"] then
    logit INFO "Exemplar Extract successful"
	exit 0
  else
	logit ERROR "Exemplar Extract failed"
    exit 1
  fi
  
  logit INFO 'Starting Copy Exemplar Files from war to tomcat dir'
  copy_Exemplar_Files 
  ret_code = $?
  if [$ret_code -eq "0"] then
    logit INFO "Copy Exemplar Files successful"
	exit 0
  else
	logit ERROR "Copy Exemplar Files failed"
    exit 1
  fi
  
  logit INFO 'Copy Exemplar Files'
  copy_Exemplar_Files 
  ret_code = $?
  if [$ret_code -eq "0"] then
    logit INFO "Exemplar Extract successful"
	exit 0
  else
	logit ERROR "Exemplar Extract failed"
    exit 1
  fi
  
  logit INFO 'Extracting Tomcat Plugins'
  extract_tc_plugins 
  ret_code = $?
  if [$ret_code -eq "0"] then
    logit INFO "Tomcat Plugins Extract successful"
	exit 0
  else
	logit ERROR "Tomcat Plugins Extract failed"
    exit 1
  fi
}