#!/bin/bash

# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

# Move Java to /opt/directory
echo 'Moving Java'
sudo mv /tmp/jdk-8u162-linux-x64.rpm /opt/

#echo "Installing java 1.8 jdk..."

#sudo yum install -y java-1.8.0-openjdk-devel

#Install and configure Java
echo 'Installing Java.....'
echo 'cd /opt/'
cd /opt/

sudo rpm -ivh jdk-8u162-linux-x64.rpm

echo 'Confirm Java version'
java -version

echo 'setting JDK Path to bashrc'
export JAVA_HOME='/opt/jdk1.8.0_151' >> ~/.bash_profile
echo $JAVA_HOME

export JRE_HOME='/opt/jdk1.8.0_151/jre' >> ~/.bash_profile
echo $JRE_HOME

echo 'setting path'
export PATH='$JAVA_HOME/bin:$PATH' >>~/.bash_profile
echo $PATH

source ~/.bash_profile
cat ~/.bash_profile


