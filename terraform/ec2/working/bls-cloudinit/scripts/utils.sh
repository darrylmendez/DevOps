#!/usr/bin/env bash
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=$(DATE + "%y-%m-%d %T")
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

#Install utils
echo 'sudo yum install -y unzip wget zip vim libaio openssl'
logit INFO "Installing unzip wget zip vim libaio openssl"
sudo yum install -y unzip wget zip vim libaio openssl
echo 'Completed Utils Installation'
logit INFO "Completed install of unzip wget zip vim libaio openssl"

