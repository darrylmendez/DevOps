#Define the init.cfg commands,
#add specific commands for adding user, groups etc in init.cfg
data "template_file" "init-script-bls" {
  template = "${file("scripts/init-bls.cfg")}"

  vars {
    REGION = "${var.AWS_REGION}"
  }
}

data "template_file" "init-script-ui" {
  template = "${file("scripts/init-ui.cfg")}"

  vars {
    REGION = "${var.AWS_REGION}"
  }
}

#Create file system and volume manager
data "template_file" "shell-script-volume" {
  template = "${file("scripts/volumes.sh")}"

  vars {
    DEVICE = "${var.INSTANCE_DEVICE_NAME}"
  }
}

#Installing JDK
#Why JDK is needed???
#data "template_file" "shell-script-jdk" {
#  template = "${file("scripts/rhel-java.sh")}"
#}

#Installing vim, wget, and libaio
data "template_file" "shell-script-utils" {
  template = "${file("scripts/utils.sh")}"
}

#Installing Oracle Client
#data "template_file" "shell-script-oracle" {
#  template = "${file("scripts/oracle-client.sh")}"
#}

data "template_cloudinit_config" "cloudinit-rlims-bls" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init-bls.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.init-script-bls.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.shell-script-volume.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.shell-script-utils.rendered}"
  }

  #   part {
  #    content_type = "text/x-shellscript"
  #    content      = "${data.template_file.shell-script-jdk.rendered}"
  #  }
  #  part {
  #   content_type = "text/x-shellscript"
  #   content      = "${data.template_file.shell-script-oracle.rendered}"
  #  }
}

data "template_cloudinit_config" "cloudinit-rlims-ui" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init-ui.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.init-script-ui.rendered}"
  }
}
