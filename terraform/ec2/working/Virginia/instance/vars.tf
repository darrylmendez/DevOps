variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "us-east-1"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-1 = "ami-66cf611b"
    us-west-1 = "ami-043f2f64"
    eu-east-2 = "ami-e82a1a8d"
  }
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskey.pub"
}
variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}
