resource "aws_key_pair" "rlimskey" {
  key_name = "rlimskey"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}
resource "aws_instance" "rlims" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.rlimskey.key_name}"
  subnet_id = "subnet-205f5508"
  
  provisioner "file" {
  source = "script.sh"
  destination="/tmp/script.sh"
	}
	provisioner "remote-exec" {
		inline = [
			 "chmod +x /tmp/script.sh",
             "sudo /tmp/script.sh"
			]
		}
	connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
  
  #provisioner "local-exec" {
  #   command = "echo ${aws_instance.rlims.private_ip} > private_ips.txt"
  #}
  

 
}





