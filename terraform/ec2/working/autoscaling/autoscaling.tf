resource "aws_launch_configuration" "rlims-launchconfig" {
  name_prefix          = "rlims-launchconfig"
  image_id             = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type        = "t2.micro"
  key_name             = "${aws_key_pair.rlimskeypair.key_name}"
  security_groups      = ["${aws_security_group.rlims.id}"]
  user_data            = "#!/bin/bash\napt-get update\napt-get -y install nginx\nMYIP=`ifconfig | grep 'addr:10' | awk '{ print $2 }' | cut -d ':' -f2`\necho 'this is: '$MYIP > /var/www/html/index.html"
  lifecycle              { create_before_destroy = true }
}

resource "aws_autoscaling_group" "rlims-autoscaling" {
  name                 = "rlims-autoscaling"
  vpc_zone_identifier  =  "vpc-0d1cf564", "vpc-0d1cf564"
  launch_configuration = "${aws_launch_configuration.rlims-launchconfig.name}"
  min_size             = 1
  max_size             = 1
  health_check_grace_period = 300
  health_check_type = "ELB"
  load_balancers = ["${aws_elb.rlims-elb.name}"]
  force_delete = true

  tag {
      key = "Name"
      value = "RLIMS ec2 instance"
      propagate_at_launch = true
  }
}

