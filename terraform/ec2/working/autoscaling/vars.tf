variable "AWS_REGION" {
  default = "us-east-2"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskey.pub"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-db2919be"
    us-west-2 = "ami-06b94666"
    eu-west-1 = "ami-844e0bf7"
  }
}
