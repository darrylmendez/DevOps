keytool -genkey -alias server -keyalg RSA -keysize 2048 -dname "CN=rlims-test.abbvienet.com, OU=BTS, O=AbbVie Inc., L=North Chicago, ST=IL, C=US" -keystore ssl.jks
keytool -importkeystore -srckeystore ssl.jks -destkeystore ssl.jks -deststoretype pkcs12
keytool -certreq -alias server -file csr.txt -keystore ssl.jks


