#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export DISCOVERY_EXTRACTOR=$1
export EXTRACTOR_NAME=$2
export DISCOVERY_GUID=$3
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/deployment
export PROGRAM_NAME=$(basename $0)
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
wait_for_file() {
  local ctr=0
  while [ ! -f $1 ]
  do
    sleep 1
	ctr=`expr $ctr + 1`
	if [ $ctr -eq 600 ];	then
	  logit ERROR "Waited too long for $1, aborting process"
	fi
 done
}
unzip_file() {
  sudo unzip "$1" -d "$2" 
}
deploy_discovery() {
  sudo mkdir $STAGE_DIR/discovery
  echo "$DEVSCI_EXTRACTOR"
  move_file $STAGE_DIR/"$DISCOVERY_EXTRACTOR" $STAGE_DIR/discovery
  sudo mkdir $STAGE_DIR/discovery/to_tomcat
  cd $STAGE_DIR/discovery
  unzip_file "$DISCOVERY_EXTRACTOR" $STAGE_DIR/discovery
  java -Dappguid=$DISCOVERY_GUID -Dtomcatpath=$STAGE_DIR/discovery/to_tomcat -jar $EXTRACTOR_NAME -donotprompt
  sudo cp $STAGE_DIR/discovery/to_tomcat/webapps/* $TOMCAT_APP_INSTALL
}
{
  set +e
  ec=0
  if [ ! -z "$DISCOVERY_GUID" ]; then
    logit INFO '------------------------------------------------'
    logit INFO 'Starting Server Extractor Discovery Install'
    logit INFO '------------------------------------------------'
    logit INFO '------------------------------------------------'
    logit INFO 'Deploy Discovery'
    logit INFO '------------------------------------------------'
    deploy_discovery
    rc=$?
    if [ $rc = "0" ]; then
      logit INFO '-----------------------------------------------'
      logit INFO 'Deployment of Discovery successful'
      logit INFO '-----------------------------------------------'
    else
  	  ec=1
      logit INFO '-----------------------------------------------'
      logit INFO "Deployment of Discovery Failed"
      logit INFO '-----------------------------------------------'
	  exit 1
    fi
  fi

  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '----------------------------------------------'
    logit INFO 'UI Server Discovery Deployment Completed.'
    logit INFO '----------------------------------------------'
	exit 0
  fi
}
