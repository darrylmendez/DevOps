<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes" />
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()" />
      </xsl:copy>
   </xsl:template>
   <xsl:template match="Service">
      <Service>
         <Connector port="443" minSpareThreads="25" disableUploadTimeout="true" scheme="https" secure="true" SSLEnabled="true" clientAuth="false" sslProtocol="TLS"  keyAlias="server" keystoreFile="/opt/tomcat/ssl-certs/ssl.jks" keystorePass="DefaultPassword"/>
         <xsl:apply-templates />
      </Service>
   </xsl:template>
</xsl:stylesheet>


