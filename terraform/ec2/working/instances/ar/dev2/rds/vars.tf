variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable "customer" {
  default = "DevSci"
}

variable "environment" {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}

variable "rds_allocated_storage" {
  default = 100
}

variable "rds_oracle_engine" {
  default = "oracle-ee"
}

variable "rds_engine_version" {
  default = "12.1.0.2.v8"
}

variable "rds_iops" {
  default = 1000
}

#variable "rds_instance_class" {
#  default = "db.t2.2xlarge"
#}

variable "rds_instance_class" {
  default = "db.t2.small"
}

variable "rds_identifier" {
  default = "rlimsd2"
}

variable "rds_db_name" {
  default = "RLIMSD2"
}

variable "rds_user" {
  default = "rlimsadmin"
}

variable "rds_password" {
  default = "AbbVie2013"
}

variable "rds_storage_type" {
  default = "io1"
}

variable "rds_backup_retention" {
  default = 30
}

variable "app-db-instance-name" {
  default = "RLIMSD2"
}

data "aws_db_snapshot" "db_snap" {
  most_recent            = true
  db_instance_identifier = "rlimsdbinstanceprod"
}

variable "oracle-parameter-name" {
  default = "default.oracle-ee-12.1"
}

variable "skip_final_snapshot" {
  default = "true"
}

variable "db_subnet_group_name" {
  default = "default-vpc-8df951e8"
}

variable "security_group_ids" {
  description = "devsci-rlims-allow-oracle"
  type        = "list"
  default = [
    "sg-021175265b180e125"
  ]
}
