variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair"
}

variable "path_to_public_key" {
  default = "rlimskeypair.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "instance_device_name" {
  default = "/dev/xvdh"
}

# This is ui base AMI
variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
  }
}

variable "guid" {
  default = ""
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.micro"
}

variable "private_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = "subnet-205f5508"
}

variable "security_group_ids" {
  description = "List of Security Group Id"
  type        = "list"

  default = [
    "sg-1fa8e954",
  ]
}

variable "public_availability_zones" {
  description = "List of potential spots for instances within a region"
  type        = "list"

  default = [
    "us-east-2a",
    "us-east-2b",
    "us-east-2c",
  ]
}

variable "ebs_volume_size" {
  default = "1"
}

variable "exemplar_lims_server_ip" {
  default = "10.239.12.49"
}

variable "email_host_ip" {
  default = "15.163.124.148"
}

variable "secondary_eni_id" {
  default = "eni-911a6fc4"
}

variable "secondary_eni_mac" {
  default = "12:8d:ac:76:77:5c"
}
