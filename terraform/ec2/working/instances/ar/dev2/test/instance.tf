resource "aws_instance" "rlims-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the VPC subnet
  subnet_id = "subnet-205f5508"

  # the security group
  vpc_security_group_ids = "${var.security_group_ids}"

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  #user_data = "${data.template_cloudinit_config.cloudinit-rlims-ui.rendered}"


  tags = {
    Name        = "${var.customer}-${var.environment}-UI"
    environment = "${var.environment}"
  }


  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-instance.private_ip} >> ui-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }
  

}


