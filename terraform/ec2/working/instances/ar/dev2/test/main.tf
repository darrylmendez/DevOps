

resource "aws_s3_bucket" "terraform-state-storage-s3" {
    bucket = "rlims-sandbox-tfstate"
    force_destroy = true
    versioning {
      enabled = true
    }
 
    tags {
      Name = "S3 Remote Terraform State Store"
    }      
}
terraform {
  backend "s3" {
    region     = "us-east-1"
    bucket     = "rlims-sandbox-tfstate"
    key        = "terraform.tfstate"
  }
}