variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev2"
}

variable "path_to_private_key" {
  default = "rlims-dev2-keypair-bls"
}

variable "path_to_public_key" {
  default = "rlims-dev2-keypair-bls.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "db_end_point" {
  default = "jdbc:oracle:thin:@rlimsdevsci.cpydef0rrtf0.us-east-1.rds.amazonaws.com:1521:devsci"
}

variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
	us-east-2 = ""
  }
}

variable "root_volume_size" {
  default = "250"
}

variable "primary_eni_id" {
  default = "eni-06a78553c47dae881"
}

variable "ping_interval" {
  default = "600000"
}

variable "callback_timeout" {
  default = "1800"
}

variable "db_compression" {
  default = "false"
}

variable "ldap_login" {
  default = "true"
}

variable "ldap_domain" {
  default = "AbbVieNet.com"
}

variable "ldap_base_dn" {
  default = "ou=People,dc=abbvienet,dc=com"
}

variable "client_embedded" {
  default = "false"
}

variable "Xmx" {
  default = "24576M"
}

variable "Xms" {
  default = "512M"
}

variable "rlims_portal_user" {
  default = "rlims_portal"
}

variable "rlims_db_password" {
  description = "Database Portal Password"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "m4.2xlarge"
}

variable "bartender_ip" {
  description = "Bartender Server IP Address"
  default     = "10.72.30.249"
}

variable "bartender_svc_account" {
  description = "Bartender Service Account"
  default     = "svc-rlimsusr"
}

variable "exemplar_base" {
  default = "exemplarlims_v1061_b777"
}

variable "exemplar_obfuscated" {
  default = "Exemplar1061ObfuscatedJar"
}

variable "exemplar_base_exec" {
  default = "exemplarlims_v1061_b777_linux"
}

variable "devsci_guid" {
  default = ""
}

variable "discovery_guid" {
  default = ""
}

variable "devsci_abbvie_zip" {
  default = "Base Delivery.zip"
}

variable "discovery_abbvie_zip" {
  default = "7 AbbVie - Primary.zip"
}

variable "devsci_install_path" {
  default = "/opt/rlims/abbvie/abbvieqa"
}

variable "discovery_install_path" {
  default = "/opt/rlims/abbvieqa/primary"
}

variable "discovery_extractor_name" {
  default = "abbvie-primary_extractor.jar"
}

variable "devsci_extractor_name" {
  default = "exemplar_extractor.jar"
}

variable "bartender_svc_password" {
  description = "Bartender Service Account"
}

