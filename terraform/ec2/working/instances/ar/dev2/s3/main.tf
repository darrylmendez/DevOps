variable "aws_region" {
  default = "us-east-1"
}

provider "aws" {
  region = "${var.aws_region}"
}
resource "aws_s3_bucket" "terraform-state-storage-s3" {
    bucket = "rlims-sandbox-tfstate"
	acl="private"
	    versioning {
      enabled = true
    }
 
    tags {
      Name = "S3 Remote Terraform State Store"
    }      
}

