variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-2"
}


variable customer {
  default = "devsci"
}

variable environment {
  description = "Name of the environment"
  default     = "rlims-dev"
}

variable "bls_private_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = ""
}

variable "bls_security_group_ids" {
  description = "List of Security Group Id"
  type        = "list"

  default = [
    ""
  ]
}

variable "ui_private_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = ""
}

variable "ui_security_group_ids" {
  description = "List of Security Group Id"
  type        = "list"

  default = [
    ""
  ]
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_network_interface" "bls" {

  subnet_id = "${var.bls_private_subnet}"
  description = "ENI for the RLIMS BLS Server"
  security_groups = "${var.bls_security_group_ids}"
  tags = {
    Name        = "${var.customer}-${var.environment}-bls"
    environment = "${var.environment}"
  }
}

resource "aws_network_interface" "ui" {
  subnet_id = "${var.ui_private_subnet}"
  description = "ENI for the RLIMS UI Server"
  security_groups = "${var.ui_security_group_ids}"
  tags = {
    Name        = "${var.customer}-${var.environment}-ui"
    environment = "${var.environment}"
  }
}
output "ui-interface-id" {
  value = "${aws_network_interface.ui.id}"
}
output "ui-interface-ip" {
  value = "${aws_network_interface.ui.private_ips}"
}
output "ui-subnet-id" {
  value = "${aws_network_interface.ui.subnet_id}"
}
output "ui-security-groups" {
  value = "${aws_network_interface.ui.security_groups}"
}
output "bls-interface-id" {
  value = "${aws_network_interface.bls.id}"
}
output "bls-interface-ip" {
  value = "${aws_network_interface.bls.private_ips}"
}
output "bls-subnet-id" {
  value = "${aws_network_interface.bls.subnet_id}"
}
output "bls-security-groups" {
  value = "${aws_network_interface.bls.security_groups}"
}




