variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "prod_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = ""
}

variable "ui_security_group_ids" {
  description = ""
  type        = "list"

  default = [
    ""
  ]
}

variable "bls_security_group_ids" {
  description = ""
  type        = "list"

  default = [
    ""
  ]
}