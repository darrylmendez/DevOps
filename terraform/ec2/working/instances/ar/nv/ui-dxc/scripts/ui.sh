#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export EXEMPLAR_BASE=$1
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/deployment
export EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/veloxClient/config/ExemplarWebClient.properties
export PORTAL_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/velox_portal/config/PortalClient.properties
export WS_CONFIG_FILE_NAME=$TOMCAT_APP_INSTALL/velox_webservice/config/VeloxWebServices.properties
export SWAGGER_JSON=$TOMCAT_APP_INSTALL/velox_webservice/docs/swagger.json
export EXEMPLAR_LIMS_SERVER_IP=$2
export EMAIL_HOST_IP=$3
export EMAIL_SENDER=rlims.support@abbvie.com
export PROGRAM_NAME=$(basename $0)
export tmpfile=/tmp/prop.$$
export TOMCAT_DIR=/opt/tomcat
export TOMCAT_CONF=$TOMCAT_DIR/conf
export TC_CONF_FILE=$TOMCAT_CONF/server.xml
export SCRIPTS_DIR=/tmp/scripts
export XSL_FILE=$SCRIPTS_DIR/transform.xsl
export commonname=`ip route get 1 | awk '{print $NF;exit}'`

error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
wait_for_file() {
  local ctr=0
  while [ ! -f $1 ]
  do
    sleep 1
	ctr=`expr $ctr + 1`
	if [ $ctr -eq 600 ];	then
	  logit ERROR "Waited too long for $1, aborting process"
	fi
 done
}
unzip_file() {
  sudo unzip "$1" -d "$2" 
}
deploy_lims_jars() {
  unzip_file $STAGE_DIR/"$EXEMPLAR_BASE" $STAGE_DIR
  sudo cp $STAGE_DIR/veloxClient/veloxClient.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/veloxclient/veloxClient.war to $TOMCAT_APP_INSTALL"
  sudo cp $STAGE_DIR/velox_portal/velox_portal.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/velox_portal/velox_portal.war to $TOMCAT_APP_INSTALL"
  sudo chown tomcat:tomcat $TOMCAT_APP_INSTALL 
}

change_group() {
  sudo chgrp $1 $2 || logit ERROR "Error changing group $1 $2"
}
change_owner() {
  sudo chown -Rf $1 $2 || logit ERROR "Error changing owner $1 $2"
}
change_owner_group() {
  sudo chown $1:$2 $3|| logit ERROR "sudo chown $1:$2 $3"
}  
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
}
configure_web_client_props() {
 wait_for_file $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME 
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email.host $EMAIL_HOST_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email_sender $EMAIL_SENDER
 echo "email.host=$EMAIL_HOST_IP" >> $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME
 echo "email.smtpport=25" >> $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME
 echo "email.sender=$EMAIL_SENDER" >> $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME
 sudo chown tomcat:tomcat /opt/tomcat
}
configure_portal_client_props() {
 wait_for_file $PORTAL_CLIENT_PROP_FILE_NAME 
 configure_props $PORTAL_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 echo "email.host=$EMAIL_HOST_IP" >> $PORTAL_CLIENT_PROP_FILE_NAME
 echo "email.smtpport=25" >> $PORTAL_CLIENT_PROP_FILE_NAME
 echo "email.sender=$EMAIL_SENDER" >> $PORTAL_CLIENT_PROP_FILE_NAME
}
config_ws_json() {
 sudo sed -i -- "s/localhost/$commonname/g" $SWAGGER_JSON
 grep host $SWAGGER_JSON
}
configure_ws_props() {
 logit INFO "Waiting for $WS_CONFIG_FILE_NAME"
 wait_for_file $WS_CONFIG_FILE_NAME 
 logit INFO "Found $WS_CONFIG_FILE_NAME"
 logit INFO "Configuring $WS_CONFIG_FILE_NAME - exemplarlims.server.ip - $EXEMPLAR_LIMS_SERVER_IP"
 configure_props $WS_CONFIG_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 grep exemplarlims.server.ip $WS_CONFIG_FILE_NAME
 config_ws_json
}
deploy_ws() {
  logit INFO "Configuring Web Services"
  logit INFO "Copy $STAGE_DIR/velox_webservice.war to $TOMCAT_APP_INSTALL/"
  sudo cp $STAGE_DIR/velox_webservice.war $TOMCAT_APP_INSTALL/ || logit ERROR "Error Copying velox_webservice.war to $TOMCAT_APP_INSTALL"
  logit INFO "Configuring Web Service Properties"
  configure_ws_props
}

{
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server Install'
  logit INFO '-------------------------------------------'
  logit INFO '-------------------------------------------'
  logit INFO 'Deploy LIMS JARS'
  logit INFO '-------------------------------------------'
  deploy_lims_jars
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO "Deployment of LIMS JARS successful"
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO "Deployment of LIMS JARS Failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Web Client Props'
  logit INFO '-------------------------------------------'
  configure_web_client_props
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Portal Client '
  logit INFO '-------------------------------------------'
  configure_portal_client_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi

  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Change ownership of tomcat '
  logit INFO '-------------------------------------------'
  change_owner_group tomcat tomcat /opt/tomcat
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Changed ownership of tomcat'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Failure to change tomcat ownerhip'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'UI Server Deployment Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
