#Define the init.cfg commands,
#add specific commands for adding user, groups etc in init.cfg
data "template_file" "init-script-ui" {
  template = "${file("scripts/init-ui.cfg")}"
}

data "template_cloudinit_config" "cloudinit-rlims-ui" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init-ui.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.init-script-ui.rendered}"
  }
}
