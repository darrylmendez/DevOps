resource "aws_instance" "rlims-bls-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${var.public_subnet}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-bls.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads/BLS"
    destination = "/tmp"
  }
  
   provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-BLS"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/bls.sh",
      "sudo /tmp/scripts/bls.sh ${var.guid}",
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-bls-instance.private_ip} > bls-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }
}

resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "us-east-1b"
  size              = "${var.ebs_volume_size}"
  type              = "gp2"

  tags {
    Name = "extra volume data"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "${var.instance_device_name}"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.rlims-bls-instance.id}"
}
