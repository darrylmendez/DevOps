variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "SSL_PASSWORD" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlims-dev-keypair-ui"
}

variable "path_to_public_key" {
  default = "rlims-dev-keypair-ui.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

# This is ui base AMI
variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
    us-east-2 = ""
  }
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "m4.2xlarge"
}

variable "exemplar_lims_server_ip" {
  default = "10.239.11.49"
}

variable "email_host_ip" {
  default = "15.163.124.148"
}

variable "primary_eni_id" {
  default = "eni-911a6fc4"
}

variable "root_volume_size" {
  default = "250"
}

variable "exemplar_base" {
  default = "exemplarlims_v1061_b777.zip"
}

variable "devsci_abbvie" {
  default = "Base Delivery.zip"
}

variable "devsci_guid" {
  default = ""
}

variable "discovery_guid" {
  default = ""
}

variable "discovery_abbvie" {
  default = "6 AbbVie - Primary.zip"
}

variable "discovery_extractor_name" {
  default = "abbvie-primary_extractor.jar"
}

variable "devsci_extractor_name" {
  default = "exemplar_extractor.jar"
}
variable "dns_name" {
  default = "rlims-sandbox.abbvienet.com"
}
