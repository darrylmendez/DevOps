output "ui-instance-ip" {
  value = "${aws_instance.rlims-ui-instance.private_ip}"
}
output "ui-instance-id" {
  value = "${aws_instance.rlims-ui-instance.id}"
}