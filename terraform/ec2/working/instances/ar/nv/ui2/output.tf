output "ui-instance-ip" {
  value = "${aws_instance.rlims-ui-instance.private_ip}"
}
