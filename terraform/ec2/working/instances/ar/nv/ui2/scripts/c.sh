#openssl genrsa -out rlims-private.pem 2048
#openssl req -new -x509 -key rlims-private.pem -out rlims-cert.pem -days 3650
#openssl pkcs12 -export -out rlims-keystore.pkcs12 -in rlims-cert.pem -inkey rlims-private.pem
$JAVA_HOME/keytool -importkeystore -srckeystore rlims-keystore.pkcs12 -srcstoretype PKCS12 -srcalias 1 -destkeystore .keystore -deststoretype JKS -destalias tomcat
