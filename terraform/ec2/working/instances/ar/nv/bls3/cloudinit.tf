#Define the init.cfg commands,
#add specific commands for adding user, groups etc in init.cfg
data "template_file" "init-script-bls" {
  template = "${file("scripts/init-bls.cfg")}"

  vars {
    REGION = "${var.aws_region}"
  }
}

#Create file system and volume manager
data "template_file" "shell-script-volume" {
  template = "${file("scripts/volumes.sh")}"

  vars {
    DEVICE = "${var.instance_device_name}"
  }
}

data "template_cloudinit_config" "cloudinit-rlims-bls" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init-bls.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.init-script-bls.rendered}"
  }

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.shell-script-volume.rendered}"
  }
}
