resource "aws_security_group" "allow-oracle" {
  vpc_id      = "${var.vpc_id}"
  name        = "allow-oracle"
  description = "allow-oracle"

  ingress {
    from_port       = 1521
    to_port         = 1521
    protocol        = "tcp"
    security_groups = ["${var.security_group_ids}"] # allowing access from our example instance
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  tags {
    Name = "devsci-rlims-allow-oracle"
  }
}

resource "aws_db_instance" "oracle" {
  allocated_storage    = "${var.rds_allocated_storage}"
  engine               = "${var.rds_oracle_engine}"
  engine_version       = "${var.rds_engine_version}"
  iops                 = "${var.rds_iops}"
  instance_class       = "${var.rds_instance_class}"
  identifier           = "${var.rds_identifier}"
  name                 = "${var.rds_db_name}"
  username             = "${var.rds_user}"
  password             = "${var.rds_password}"
  db_subnet_group_name = "${var.db_subnet_group_name}"

  #snapshot_identifier  = "${data.aws_db_snapshot.db_snap.id}"
  #snapshot_identifier = "${var.snapshot-identifier}"
  parameter_group_name = "${var.oracle-parameter-name}"

  multi_az                = "false"
  vpc_security_group_ids  = ["${aws_security_group.allow-oracle.id}"]
  storage_type            = "${var.rds_storage_type}"
  backup_retention_period = "${var.rds_backup_retention}"
  availability_zone       = "${var.az}"
  skip_final_snapshot     = "${var.skip_final_snapshot}"

  tags {
    Name = "${var.app-db-instance-name}"
  }

  timeouts {
    create = "5h"
    delete = "2h"
  }
}

output "rds_endpoint" {
  value = "${aws_db_instance.oracle.endpoint}"
}
