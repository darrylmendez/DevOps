variable "rds_allocated_storage" {
  default = 100
}

variable "rds_oracle_engine" {
  default = "oracle-ee"
}

variable "rds_engine_version" {
  default = "12.1.0.2.v8"
}

variable "rds_iops" {
  default = 1000
}

variable "rds_instance_class" {
  default = "db.m4.2xlarge"
}

variable "rds_identifier" {
  default = "rlimsd2"
}

variable "rds_db_name" {
  default = "RLIMSD2"
}

variable "rds_user" {
  default = "rlimsadmin"
}

variable "rds_password" {
  default = "AbbVie2013"
}

variable "rds_storage_type" {
  default = "io1"
}

variable "rds_backup_retention" {
  default = 30
}

variable "app-db-instance-name" {
  default = "RLIMSD2"
}

data "aws_db_snapshot" "db_snap" {
  most_recent            = true
  db_instance_identifier = "rlimsdbinstanceprod"
}

variable "oracle-parameter-name" {
  default = "oracle-db-parameters"
}

variable "skip_final_snapshot" {
  default = "true"
}

variable "db_subnet_group_name" {
  default = "default-vpc-8df951e8"
}

resource "aws_db_instance" "oracle" {
  allocated_storage    = "${var.rds_allocated_storage}"
  engine               = "${var.rds_oracle_engine}"
  engine_version       = "${var.rds_engine_version}"
  iops                 = "${var.rds_iops}"
  instance_class       = "${var.rds_instance_class}"
  identifier           = "${var.rds_identifier}"
  name                 = "${var.rds_db_name}"
  username             = "${var.rds_user}"
  password             = "${var.rds_password}"
  db_subnet_group_name = "${var.db_subnet_group_name}"
  snapshot_identifier  = "${data.aws_db_snapshot.db_snap.id}"

  #snapshot_identifier = "${var.snapshot-identifier}"

  parameter_group_name    = "${var.oracle-parameter-name}"
  multi_az                = "false"
  vpc_security_group_ids  = ["${var.security_group_ids}"]
  storage_type            = "${var.rds_storage_type}"
  backup_retention_period = "${var.rds_backup_retention}"
  availability_zone       = "${var.az}"
  skip_final_snapshot     = "${var.skip_final_snapshot}"
  tags {
    Name = "${var.app-db-instance-name}"
  }
  timeouts {
    create = "5h"
    delete = "2h"
  }
}
