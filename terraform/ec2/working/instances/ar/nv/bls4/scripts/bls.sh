#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 03/28/2018
# Description	: Initial Revision
# Define Variables
export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export EXEMPLAR_LICENSE=exemplar.license
export VELOX_SERVER=$EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/ 
export PROGRAM_NAME=`basename "$0"`
export EXEMPLAR_LIMS_DELIVERY="AbbVie Delivery 6"
export STAGE_DIR=/tmp/bls
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [! -d $1]; then
    exit 1
  fi 
}
install_license() {
  logit INFO "Moving Exemplar License file to "$EXEMPLAR_INSTALL_DIR 
  move_file $STAGE_DIR/"$EXEMPLAR_LICENSE" $EXEMPLAR_INSTALL_DIR || logit ERROR "Move File Exemplar License $EXEMPLAR_LICENSE to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Moved Exemplar License file to "$EXEMPLAR_INSTALL_DIR		

}
configure_velox_server_props() {
  sudo cp $STAGE_DIR/VeloxServer.properties $EXEMPLAR_INSTALL_DIR 
}
apply_lims_delivery() {
  move_file $STAGE_DIR/"$EXEMPLAR_LIMS_DELIVERY.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar LIMS Delivery $EXEMPLAR_LIMS_DELIVERY.zip to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Unzip file ""$EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR
  unzip_file $EXEMPLAR_INSTALL_DIR/"$EXEMPLAR_LIMS_DELIVERY".zip $EXEMPLAR_INSTALL_DIR
  #unzip_file $STAGE_DIR/"$EXEMPLAR_LIMS_DELIVERY".zip $EXEMPLAR_INSTALL_DIR
  logit INFO "Unzipped file ""$EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR 
}
run_DatamgmtServer() {
 logit INFO 'Starting DataMgmtServer.bsh'
 sudo -u rlimsadmin ./DataMgmtServer.bsh &
 logit INFO "DataMgmt Server Process id: " `ps aux | grep [D]ataMgmtServer.bsh | awk '{print $2}'`
}

# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  ec=0
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting BLS App Install'
  logit INFO '-------------------------------------------'	
  
  logit INFO 'Installing Licenses'
  install_license
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Installed License'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'License Install Failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
 
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting Configure Velox Server Properties'
  logit INFO '-------------------------------------------'	  
  configure_velox_server_props
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Velox Server Properties configured'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'Velox Server Properties configured failed'
    logit INFO '-------------------------------------------'	  
	exit 1
  fi
  
  logit INFO '-------------------------------------------'	
  logit INFO 'Applying LIMS Delivery Patch'
  logit INFO '-------------------------------------------'	  
  apply_lims_delivery 
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'LIMS Delivery Patch Applied'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'LIMS Delivery Patch Failed'
    logit INFO '-------------------------------------------'	  
	exit 1
  fi
  logit INFO '-------------------------------------------'	
  logit INFO 'Skipping DatamgmtServer Sapio License issue '
  logit INFO '-------------------------------------------'	  
  
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting DatamgmtServer'
  logit INFO '-------------------------------------------'	  
  run_DatamgmtServer 
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Started'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Failed'
    logit INFO '-------------------------------------------'	  
  fi
  logit INFO '-------------------------------------------'	
  logit INFO 'BLS App Components Installed'
  logit INFO '-------------------------------------------'	
}