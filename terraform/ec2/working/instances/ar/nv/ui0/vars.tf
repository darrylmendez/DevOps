variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair"
}

variable "path_to_public_key" {
  default = "rlimskeypair.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "instance_device_name" {
  default = "/dev/xvdh"
}

# This is ui base AMI
variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-56320a29"
  }
}

variable "guid" {
  default = "813a36c9-c0c7-4eb7-8261-2c12f03826cc"
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}
variable "instance_type" {
  description = "Size of the instance"
  default = "t2.micro"
}
variable "public_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = "subnet-577ab320"
}

variable "private_subnets" {
  description = "Available private subnet ids for availability zones"
  type        = "list"

  default = [
    "subnet-577ab320"
  ]
}

variable "security_group_ids" {
  description = "List of Security Group Id"
  type = "list"
  default = [
	"sg-1fa8e954"
	]
	}
	
variable "private_cidrs" {
  description = "List of ip blocks that correlate to the zones"
  type        = "list"

  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24",
    "10.0.4.0/24",
    "10.0.5.0/24",
    "10.0.6.0/24",
  ]
}

variable "public_availability_zones" {
  description = "List of potential spots for instances within a region"
  type        = "list"

  default = [
    "us-east-2a",
    "us-east-2b",
    "us-east-2c",
  ]
}

variable "public_cidrs" {
  description = "List of ip blocks that correlate to the zones"
  type        = "list"

  default = [
    "10.0.101.0/24",
    "10.0.102.0/24",
    "10.0.103.0/24",
    "10.0.104.0/24",
    "10.0.105.0/24",
    "10.0.106.0/24",
  ]
}

// Security
variable "allowed_external_networks" {
  description = "Array of allowed source networks"

  default = [
    "0.0.0.0",
  ]

  type = "list"
}

variable "ebs_volume_size" {
  default = "1"
}
