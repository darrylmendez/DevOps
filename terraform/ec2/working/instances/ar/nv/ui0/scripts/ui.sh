#!/bin/bash
export ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/common
export GUID=$1
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
start_tc() {
  sudo /bin/su -p -s /bin/sh tomcat /opt/tomcat/bin/startup.sh
}
abbvie_delivery_extractor() {
  sudo java -Dappguid=$GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR
}
{
  logit INFO '-------------------------------'
  logit INFO 'Starting UI Server App Install'
  logit INFO '-------------------------------'
  
  logit INFO '-----------------------------------'
  logit INFO 'Start Tomcat '
  logit INFO '-----------------------------------'
  start_tc
  logit INFO '-----------------------------------'
  logit INFO 'Tomcat Started'
  logit INFO '-----------------------------------'
   
  logit INFO '-----------------------------------'
  logit INFO 'Starting abbvie_delivery_extractor '
  logit INFO '-----------------------------------'
  abbvie_delivery_extractor
  logit INFO '-----------------------------------'
  logit INFO 'AbbVie Delivery Extractor completed'
  logit INFO '-----------------------------------'

  
  logit INFO '---------------------------------'
  logit INFO 'Completed UI Server Base Install'
  logit INFO '---------------------------------'
}
