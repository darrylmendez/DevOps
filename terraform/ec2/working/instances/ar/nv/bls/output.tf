output "bls-instance-ip" {
  value = "${aws_instance.rlims-bls-instance.private_ip}"
}

output "bls-instance-id" {
  value = "${aws_instance.rlims-bls-instance.id}"
}

