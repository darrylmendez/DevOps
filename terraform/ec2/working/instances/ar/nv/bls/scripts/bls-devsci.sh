#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 10/01/2018
# Description	: Initial Revision
# Define Variables
export DEVSCI_EXTRACTOR_ZIP=$1
export DEVSCI_EXTRACTOR_NAME=$2
export DEVSCI_INSTALL_PATH=$3
export DEVSCI_GUID=$4
export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export PROGRAM_NAME=`basename "$0"`
export STAGE_DIR=/tmp/deployment
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
}
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}
 
deploy_devsci() {
 cd $STAGE_DIR
 sudo mkdir devsci_extractor
 unzip_file $DEVSCI_EXTRACTOR_ZIP $STAGE_DIR/devsci_extractor
 if [ ! -d $DEVSCI_INSTALL_PATH ]; then
    sudo mkdir $DEVSCI_INSTALL_PATH
 fi  
 sudo cp -r $STAGE_DIR/devsci_extractor $DEVSCI_INSTALL_PATH || logit ERROR "Unable to copy files from $STAGE_DIR/devsci_extractor/ to $DEVSCI_INSTALL_PATH"
 cd $DEVSCI_INSTALL_PATH
 sudo java -Dappguid=$DEVSCI_GUID -jar $DEVSCI_INSTALL_PATH/$DEVSCI_EXTRACTOR_NAME -donotprompt
 sudo chown -R rlimsadmin:rlimsadmin $EXEMPLAR_INSTALL_DIR
}

# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set +e
  ec=0
  if [ ! -z "$DEVSCI_GUID" ]; then
    logit INFO '----------------------------------------------'
    logit INFO 'Starting BLS Server Extractor DEV SCI Install'
    logit INFO '-----------------------------------------------'
    logit INFO '-----------------------------------------------'
    logit INFO 'Deploy Dev SCI'
    logit INFO '-----------------------------------------------'
    deploy_devsci
    rc=$?
    if [ $rc = "0" ]; then
      logit INFO '----------------------------------------------'
      logit INFO 'Deployment of Dev Science successful'
      logit INFO '----------------------------------------------'
    else
  	  ec=1
      logit INFO '----------------------------------------------'
      logit INFO "Deployment of Dev Science Failed"
      logit INFO '----------------------------------------------'
	  exit 1
    fi
  fi

}