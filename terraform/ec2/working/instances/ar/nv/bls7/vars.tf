variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair-bls"
}

variable "path_to_public_key" {
  default = "rlimskeypair-bls.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "db_end_point" {
  default = "rlimsd2.cpydef0rrtf0.us-east-1.rds.amazonaws.com"
}

variable "instance_device_name" {
  default = "/dev/xvdh"
}

variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-010bd55b40be4e7e6"
  }
}

variable "guid" {
  default = "813a36c9-c0c7-4eb7-8261-2c12f03826cc"
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}

variable "private_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = "subnet-205f5508"
}

variable "public_availability_zones" {
  description = "List of potential spots for instances within a region"
  type        = "list"

  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
  ]
}

variable "ebs_volume_size" {
  default = "100"
}

variable "root_volume_size" {
  default = "100"
}

variable "secondary_eni_id" {
  default = "eni-61cd2675"
}

variable "security_group_ids" {
  description = "allow-ssh"
  type        = "list"

  default = [
    "sg-1fa8e954",
  ]
}

variable "ping_interval" {
  default = "600000"
}

variable "callback_timeout" {
  default = "600"
}

variable "db_compression" {
  default = "false"
}

variable "ldap_login" {
  default = "true"
}

variable "ldap_domain" {
  default = "AbbVieNet.com"
}

variable "ldap_url" {
  default = "ldaps://10.72.249.44:636"
}

variable "ldap_base_dn" {
  default = "ou=People,dc=abbvienet,dc=com"
}

variable "client_embedded" {
  default = "false"
}

variable "Xmx" {
  default = "8192M"
}

variable "Xms" {
  default = "256M"
}

variable "rlims_portal_user" {
  default = "rlimsportal"
}

variable "rlims_db_password" {
  default = "AbbVie2013"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.micro"
}

variable "bartender_ip" {
  description = "Bartender Server IP Address"
  default     = "10.72.30.249"
}

variable "bartender_svc_account" {
  description = "Bartender Service Account"
  default     = "svc-rlimsusr"
}

variable "bartender_svc_password" {
  description = "Bartender Service Account"
}
