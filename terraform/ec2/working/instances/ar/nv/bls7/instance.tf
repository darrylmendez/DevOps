resource "aws_instance" "rlims-bls-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-bls.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads/bls/app"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-bls"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/bls.sh",
      "sudo /tmp/scripts/bls.sh ${var.rlims_portal_user} ${var.rlims_db_password} ${var.Xmx} ${var.Xms} ${var.db_end_point} ${var.ping_interval} ${var.callback_timeout} ${var.db_compression} ${var.ldap_login} ${var.ldap_base_dn} ${var.client_embedded}",
      "chmod +x /tmp/scripts/bartender.sh",
      "sudo /tmp/scripts/bartender.sh ${var.bartender_ip} ${var.bartender_svc_account} ${var.bartender_svc_password}",
      "chmod +x /tmp/scripts/oracle_client.sh",
      "sudo /tmp/scripts/oracle_client.sh",
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-bls-instance.private_ip} > bls-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }

  network_interface {
    network_interface_id  = "${var.secondary_eni_id}"
    delete_on_termination = false
    device_index          = 0
  }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.root_volume_size}"
    delete_on_termination = true
  }
}

resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "${var.az}"
  size              = "${var.ebs_volume_size}"
  type              = "gp2"

  tags {
    Name        = "${var.customer}-${var.environment}-bls-vol"
    environment = "${var.environment}"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "${var.instance_device_name}"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.rlims-bls-instance.id}"
}
