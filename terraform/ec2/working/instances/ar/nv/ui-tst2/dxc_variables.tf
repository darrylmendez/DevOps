variable "region" {
  type = "string"
  description = "The AWS region to create resources in."
  default = "us-east-2"
}

variable "type" {
  type = "string"
  description = "The EC2 instance type for the server"
}

variable "ami" {
  type = "string"
  description = "The EC2 ami id"
}

variable "s3_bucket" {
  type = "string"
  description = "The S3 bucket in the EC2 instance's region where DXC MSP - executable software packages are present"
}

variable "key_name" {
  type = "string"
  description = "The name of the SSH key to use on instances"
}
