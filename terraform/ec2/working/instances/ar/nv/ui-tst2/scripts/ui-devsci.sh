#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export DEVSCI_EXTRACTOR=$1
export DEVSCI_GUID=$2
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/deployment
export PROGRAM_NAME=$(basename $0)
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
wait_for_file() {
  local ctr=0
  while [ ! -f $1 ]
  do
    sleep 1
	ctr=`expr $ctr + 1`
	if [ $ctr -eq 600 ];	then
	  logit ERROR "Waited too long for $1, aborting process"
	fi
 done
}
unzip_file() {
  sudo unzip "$1" -d "$2" 
}
deploy_devsci() {
  sudo mkdir $STAGE_DIR/devsci
  echo "$DEVSCI_EXTRACTOR"
  sudo mv $STAGE_DIR/"$DEVSCI_EXTRACTOR" $STAGE_DIR/devsci
  sudo mkdir $STAGE_DIR/devsci/to_tomcat
  cd $STAGE_DIR/devsci
  unzip_file "$DEVSCI_EXTRACTOR" $STAGE_DIR/devsci
  java -Dappguid=$DEVSCI_GUID -Dtomcatpath=$STAGE_DIR/devsci/to_tomcat -jar exemplar_10.6.0_extractor.jar -donotprompt
  sudo cp $STAGE_DIR/devsci/to_tomcat/webapps/* $TOMCAT_APP_INSTALL
}

{
  set +e
  ec=0
  if [ -z "$DEVSCI_GUID" ]; then
     logit ERROR "No DevSci Guid's available aborting"
  fi
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server Extractor Install'
  logit INFO '-------------------------------------------'
  logit INFO '-------------------------------------------'
  logit INFO 'Deploy Dev SCI'
  logit INFO '-------------------------------------------'
  deploy_devsci
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO 'Deployment of Dev Science successful'
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO "Deployment of Dev Science Failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
 
  
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'UI Server Dev SCI Deployment Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
