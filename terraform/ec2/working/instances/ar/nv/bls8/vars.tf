variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair-bls"
}

variable "path_to_public_key" {
  default = "rlimskeypair-bls.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "db_end_point" {
  default = "jdbc:oracle:thin:@rlimsd2.cpydef0rrtf0.us-east-1.rds.amazonaws.com:1521:RLIMSD2"
}

variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
  }
}

variable "root_volume_size" {
  default = "250"
}

variable "primary_eni_id" {
  default = "eni-61cd2675"
}

variable "ping_interval" {
  default = "600000"
}

variable "callback_timeout" {
  default = "600"
}

variable "db_compression" {
  default = "false"
}

variable "ldap_login" {
  default = "true"
}

variable "ldap_domain" {
  default = "AbbVieNet.com"
}

variable "ldap_base_dn" {
  default = "ou=People,dc=abbvienet,dc=com"
}

variable "client_embedded" {
  default = "false"
}

variable "Xmx" {
  default = "8192M"
}

variable "Xms" {
  default = "256M"
}

variable "rlims_portal_user" {
  default = "rlimsportal"
}

variable "rlims_db_password" {
  default = "AbbVie2013"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.xlarge"
}

variable "bartender_ip" {
  description = "Bartender Server IP Address"
  default     = "10.72.30.249"
}

variable "bartender_svc_account" {
  description = "Bartender Service Account"
  default     = "svc-rlimsusr"
}

variable "bartender_svc_password" {
  description = "Bartender Service Account"
}

variable "exemplar_base" {
  default = "exemplarlims_v1060_b770"
}

variable "exemplar_obfuscated" {
  default = "Exemplar1060ObfuscatedJar"
}

variable "exemplar_base_exec" {
  default = "exemplarlims_v1060_b770_linux"
}
variable "ldap_domain_name" {
  default="AbbVieNet.com"
 }
