
resource "aws_instance" "track-dev" {
 ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "t2.xlarge"

  # the VPC subnet
  subnet_id = "${var.private_subnet}"


  # the security group
  vpc_security_group_ids = ["${var.security_group_ids}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"
  
  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-bls.rendered}"
  provisioner "file" {
    source      = "/home/ubuntu/DownLoads/bls/app"
    destination = "/tmp"
  }
  provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }
  tags = {
    Name        = "${var.customer}-${var.environment}-bls"
    environment = "${var.environment}"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/bls.sh",
      "sudo /tmp/scripts/bls.sh ${var.guid} ${var.db_end_point} ${var.ping_interval} ${var.callback_timeout} ${var.db_compression} ${var.ldap_login} ${var.ldap_base_dn} ${var.client_embedded}",
    ]
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-bls-instance.private_ip} > bls-private_ips.txt"
  }
  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }
  network_interface {
    network_interface_id  = "${var.secondary_eni_id}"
    delete_on_termination = false
    device_index          = 0
  }
}

resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "${var.az}"
  size              = "${var.ebs_volume_size}"
  type              = "gp2"

  tags {
    Name = "extra volume data"
  }


} 