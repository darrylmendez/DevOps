#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
# Define Variables
export rlims_portal=$1
export rlims_password=$2
export jdbc=$3
export ping_interval=$4
export callback_timeout=$5
export ldap_login=$6
export ldap_domain=$7
export ldap_base_dn=$8
export client_embedded=$9

export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export EXEMPLAR_LICENSE=exemplar.license
export PROGRAM_NAME=`basename "$0"`
export STAGE_DIR=/tmp/deployment
export PROP_FILE_NAME=$EXEMPLAR_INSTALL_DIR/VeloxServer.properties
      
export tmpfile=/tmp/prop.$$
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
#configure key pair values 
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
} 
#configure key pair values 
configure_ldap_props() {
  local file_name=$1
  sudo sed '/^ldap.url/s/=.*$/=ldap:\/\/10.72.249.44:389/' $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
} 
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}
 
configure_velox_server_props() {
  logit INFO $jdbc
  configure_props $PROP_FILE_NAME exemplarlims.systemdb.url $jdbc
  logit INFO "ping_interval : $ping_interval"
  configure_props $PROP_FILE_NAME exemplarlims.pinginterval $ping_interval
  logit INFO "callback_timeout : $callback_timeout"
  configure_props $PROP_FILE_NAME callback.timeout $callback_timeout
  logit INFO "callback_timeout : $ldap_login"
  configure_props $PROP_FILE_NAME ldap.login $ldap_login
  logit INFO "ldap_domain : $ldap_domain"
  configure_props $PROP_FILE_NAME ldap.domainName $ldap_domain
  logit INFO "ldap_base_dn : $ldap_base_dn"
  configure_props $PROP_FILE_NAME ldap.base.dn $ldap_base_dn  
  logit INFO "client_embedded: $client_embedded"
  configure_props $PROP_FILE_NAME client.embedded $client_embedded
  configure_ldap_props $PROP_FILE_NAME
  echo 'ldap.log=true' >> $PROP_FILE_NAME
}
run_expect() {
  cd $EXEMPLAR_INSTALL_DIR
  sudo cp /tmp/scripts/script.exp $EXEMPLAR_INSTALL_DIR
  sudo chown rlimsadmin:rlimsadmin script.exp
  sudo chmod 744 $EXEMPLAR_INSTALL_DIR/script.exp 
  sudo yum install -y expect || logit ERROR "sudo yum install expect"
  sudo usermod rlimsadmin -s /bin/bash
  #logit INFO "Username/Password: $rlims_portal/$rlims_password"
  #sudo su rlimsadmin
  nohup sudo -u rlimsadmin ./script.exp $rlims_portal $rlims_password &
  logit INFO "DataMgmt Server Process id: " `ps aux | grep [D]ataMgmtServer.bsh | awk '{print $2}'`
}
run_DatamgmtServer() {
 run_expect
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  ec=0
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting BLS Install'
  logit INFO '-------------------------------------------'	
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting Configure Velox Server Properties'
  logit INFO '-------------------------------------------'	  
  configure_velox_server_props
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Velox Server Properties configured'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'Velox Server Properties configured failed'
    logit INFO '-------------------------------------------'	  
	exit 1
  fi	
  
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting DatamgmtServer'
  logit INFO '-------------------------------------------'	  
  run_DatamgmtServer 
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Started'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Failed'
    logit INFO '-------------------------------------------'	  
	exit 1
  fi

}