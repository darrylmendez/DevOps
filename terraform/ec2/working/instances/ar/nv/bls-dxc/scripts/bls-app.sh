#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
# Define Variables
export EXEMPLAR_INSTALL_DIR=/opt/rlims
export EXEMPLAR_BASE=$1
export EXEMPLAR_OBFUSCATED_JAR=$2
export EXEMPLAR_BASE_LINUX_EXEC=$3
export Xmx=$4
export Xms=$5
export EXEMPLAR_LICENSE=exemplar.license
export VELOX_SERVER=$EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/ 
export PROGRAM_NAME=`basename "$0"`
export STAGE_DIR=/tmp/deployment
export PROP_FILE_NAME=VeloxServer.properties
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi

}
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip "$1" -d "$2"
}
config_DataMgmtBsh() {
  sed -i -- "s/Xmx[0-9]*M/Xmx$Xmx/g" $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh || logit ERROR "sed -i -- s/Xmx[0-9]*M/$Xmx/g"
  sed -i -- "s/Xms[0-9]*M/Xms$Xms/g" $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh || logit ERROR "sed -i -- s/Xms[0-9]*M/$Xms/g"
}
exemplar_obfuscated() {
  if [ -f $STAGE_DIR/"$EXEMPLAR_OBFUSCATED_JAR.zip" ]; then
    move_file $STAGE_DIR/"$EXEMPLAR_OBFUSCATED_JAR.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar Obfuscated $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR Failed"
    unzip_file $EXEMPLAR_INSTALL_DIR/"$EXEMPLAR_OBFUSCATED_JAR".zip $EXEMPLAR_INSTALL_DIR/ || logit ERROR "Unzip File $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR Failed"	
    sudo cp -R $EXEMPLAR_INSTALL_DIR/veloxserver/* $EXEMPLAR_INSTALL_DIR
    sudo chmod +x $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh 
    sudo chown -R rlimsadmin:rlimsadmin $EXEMPLAR_INSTALL_DIR
	if [ -f $VELOX_SERVER/$PROP_FILE_NAME ]; then
	  sudo cp $VELOX_SERVER/$PROP_FILE_NAME $EXEMPLAR_INSTALL_DIR || logit ERROR "sudo cp $VELOX_SERVER/$PROP_FILE_NAME $EXEMPLAR_INSTALL_DIR"
	else
      logit ERROR "$VELOX_SERVER/$PROP_FILE_NAME Property File Not Found"
    fi
	config_DataMgmtBsh
  fi
}
install_exemplar_base() {
  if [ -f $STAGE_DIR/"$EXEMPLAR_BASE.zip" ]; then
    move_file $STAGE_DIR/"$EXEMPLAR_BASE.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar Base $EXEMPLAR_BASE.zip to $EXEMPLAR_INSTALL_DIR Failed"
    cd $EXEMPLAR_INSTALL_DIR 
    sudo unzip "$EXEMPLAR_BASE".zip -d $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/ 
  fi
}
install_license() {
  if [ -f $STAGE_DIR/"$EXEMPLAR_LICENSE" ]; then
    logit INFO "Moving Exemplar License file to "$EXEMPLAR_INSTALL_DIR 
    move_file $STAGE_DIR/"$EXEMPLAR_LICENSE" $EXEMPLAR_INSTALL_DIR || logit ERROR "Move File Exemplar License $EXEMPLAR_LICENSE to $EXEMPLAR_INSTALL_DIR Failed"
    logit INFO "Moved Exemplar License file to "$EXEMPLAR_INSTALL_DIR	
  fi
}
exemplar_install() {
  if [ ! -d "$EXEMPLAR_INSTALL_DIR" ]; then
    sudo mkdir $EXEMPLAR_INSTALL_DIR || logit ERROR "Exemplar Install Directory $EXEMPLAR_INSTALL_DIR Creation failed"
  fi
  install_license
  install_exemplar_base
}
	
install_Velox_Server() {
  cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver || logit ERROR "cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver failed"
  sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC 	|| logit ERROR "sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC failed"
  echo $EXEMPLAR_INSTALL_DIR | sudo ./$EXEMPLAR_BASE_LINUX_EXEC || logit ERROR "Installation of Exemplar LIMS Base Failed at $EXEMPLAR_INSTALL_DIR by $EXEMPLAR_BASE_LINUX_EXEC"
}

change_ownership() {
  logit INFO "Change Ownership " "$1":"$2" $EXEMPLAR_INSTALL_DIR
  sudo chown -R "$1":"$2" $EXEMPLAR_INSTALL_DIR || logit ERROR "Change Ownership failed $1:$2 - $EXEMPLAR_INSTALL_DIR" 
  logit INFO "Change Ownership Successful " "$1":"$2" $EXEMPLAR_INSTALL_DIR
}
verify_directory() {
  if [! -d $1]; then
    exit 1
  fi 
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set +e
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Exemplar Install'
  logit INFO '-------------------------------------------'  
  exemplar_install
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
	logit INFO "Exemplar install successful"
    logit INFO '-------------------------------------------'
  else
   	ec=1
    logit INFO '-------------------------------------------'
	logit ERROR "Exemplar install failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Velox Server Install'
  logit INFO '-------------------------------------------'
  install_Velox_Server
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
	logit INFO "Velox Server install successful"
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
	logit INFO "Velox Server install failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Change Ownership of rlimsadmin'
  logit INFO '-------------------------------------------'	
  change_ownership rlimsadmin rlimsadmin
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
	logit INFO "Change Owner successful"
    logit INFO '-------------------------------------------'		
  else
    logit INFO '-------------------------------------------'
	logit INFO "Change Owner failed"
    logit INFO '-------------------------------------------'		
    exit 1
  fi
  logit INFO '-------------------------------------------'
  logit INFO 'Starting install of obfuscated'
  logit INFO '-------------------------------------------'	
  exemplar_obfuscated
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
	logit INFO "Exemplar Obfuscated installed"
    logit INFO '-------------------------------------------'		
  else
    logit INFO '-------------------------------------------'
	logit INFO "Exemplar Obfuscated failed"
    logit INFO '-------------------------------------------'		
    exit 1
  fi
}