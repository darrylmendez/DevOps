data "template_file" "user_data" {
  template = "${file("${path.module}/templates/user_data/user_data.sh.template")}"

  vars {
    s3_bucket = "${var.s3_bucket}"
  }
}
