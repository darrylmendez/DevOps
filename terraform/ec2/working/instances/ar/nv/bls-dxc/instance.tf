# RLIMS Instance provisioning 
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/01/2018
# Description	: Initial Revision

resource "aws_instance" "rlims-bls-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_file.user_data.rendered}"


  provisioner "file" {
    source      = "/data/rlims/deployment"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-bls"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/bls-base.sh",
      "chmod +x /tmp/scripts/bls-app.sh",
      "chmod +x /tmp/scripts/bls.sh",
      "chmod +x /tmp/scripts/oracle_client.sh",
      "chmod +x /tmp/scripts/bartender.sh",
      "chmod +x /tmp/scripts/bls-devsci.sh",
      "chmod +x /tmp/scripts/bls-discovery.sh",
      "sudo /tmp/scripts/bls-base.sh",
      "sudo /tmp/scripts/bls-app.sh ${var.exemplar_base} ${var.exemplar_obfuscated} ${var.exemplar_base_exec} ${var.Xmx} ${var.Xms}",
      "sudo /tmp/scripts/bls.sh ${var.rlims_portal_user} ${var.rlims_db_password} ${var.db_end_point} ${var.ping_interval} ${var.callback_timeout} ${var.ldap_login} ${var.ldap_domain} ${var.ldap_base_dn} ${var.client_embedded}",
      "sudo /tmp/scripts/bartender.sh ${var.bartender_ip} ${var.bartender_svc_account} ${var.bartender_svc_password}",
      "sudo /tmp/scripts/oracle_client.sh",
      "sudo /tmp/scripts/bls-devsci.sh \"${var.devsci_abbvie_zip}\" ${var.devsci_extractor_name} ${var.devsci_install_path} ${var.devsci_guid}",
      "sudo /tmp/scripts/bls-discovery.sh \"${var.discovery_abbvie_zip}\" ${var.discovery_extractor_name} ${var.discovery_install_path} ${var.discovery_guid}"
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-bls-instance.private_ip} > bls-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }

  network_interface {
    network_interface_id  = "${var.primary_eni_id}"
    delete_on_termination = false
    device_index          = 0
  }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.root_volume_size}"
    delete_on_termination = true
  }
}
