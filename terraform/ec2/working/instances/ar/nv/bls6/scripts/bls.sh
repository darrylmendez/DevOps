#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
# Define Variables
export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export EXEMPLAR_LICENSE=exemplar.license
export VELOX_SERVER=$EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/ 
export PROGRAM_NAME=`basename "$0"`
export EXEMPLAR_LIMS_DELIVERY="AbbVie Delivery 6"
export STAGE_DIR=/tmp/app
export PROP_FILE_NAME=$STAGE_DIR/VeloxServer.properties
export jdbc=$1
export ping_interval=$2
export callback_timeout=$3
export db_compression=$4
export ldap_login=$5
export ldap_domain=$6
export ldap_url=$7
export ldap_base_dn=$8
export client_embedded=$9
export tmpfile=/tmp/prop.$$
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
#configure key pair values 
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
} 
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}
install_license() {
  logit INFO "Moving Exemplar License file to "$EXEMPLAR_INSTALL_DIR 
  move_file $STAGE_DIR/"$EXEMPLAR_LICENSE" $EXEMPLAR_INSTALL_DIR || logit ERROR "Move File Exemplar License $EXEMPLAR_LICENSE to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Moved Exemplar License file to "$EXEMPLAR_INSTALL_DIR		

}
configure_velox_server_props() {
  if [ -f $PROP_FILE_NAME ]; then
    sudo cp $PROP_FILE_NAME $EXEMPLAR_INSTALL_DIR 
    configure_props $PROP_FILE_NAME exemplarlims.systemdb.url $jdbc
    configure_props $PROP_FILE_NAME exemplarlims.pinginterval $ping_interval
    configure_props $PROP_FILE_NAME callback.timeout $callback_timeout
    configure_props $PROP_FILE_NAME exemplarlims.dbtablecompression $db_compression
    configure_props $PROP_FILE_NAME ldap.login $ldap_login
    configure_props $PROP_FILE_NAME ldap.domainName $ldap_domain
    #configure_props $PROP_FILE_NAME ldap.url $ldap_url
    #configure_props $PROP_FILE_NAME ldap.base.dn $ldap_base_dn
    configure_props $PROP_FILE_NAME client.embedded $client_embedded
  fi
}
run_DatamgmtServer() {
 logit INFO 'Starting DataMgmtServer.bsh'
 cd $EXEMPLAR_INSTALL_DIR
 sudo -u rlimsadmin ./DataMgmtServer.bsh & 
 logit INFO "DataMgmt Server Process id: " `ps aux | grep [D]ataMgmtServer.bsh | awk '{print $2}'`
}
apply_lims_delivery() {
  move_file $STAGE_DIR/"$EXEMPLAR_LIMS_DELIVERY.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar LIMS Delivery $EXEMPLAR_LIMS_DELIVERY.zip to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Unzip file $EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR
  unzip_file $EXEMPLAR_INSTALL_DIR/"$EXEMPLAR_LIMS_DELIVERY".zip $EXEMPLAR_INSTALL_DIR
  logit INFO "Unzipped file $EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR 
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  ec=0
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting BLS App Install'
  logit INFO '-------------------------------------------'	
  if [ $STAGE_DIR/"$EXEMPLAR_LICENSE" ]; then
    logit INFO 'Installing Licenses'
    install_license
    rc=$?
    if [ $rc = "0" ]; then
      logit INFO '-------------------------------------------'	
      logit INFO 'Installed License'
      logit INFO '-------------------------------------------'	
    else
      ec=1
      logit INFO '-----------------------------------------------'
      logit INFO 'License Install Failed'
      logit INFO '-----------------------------------------------'
      exit 1
    fi
  fi 
  if [ -f $PROP_FILE_NAME ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Starting Configure Velox Server Properties'
    logit INFO '-------------------------------------------'	  
    configure_velox_server_props
    rc=$?  
    if [ $rc = "0" ]; then
      logit INFO '-------------------------------------------'	
      logit INFO 'Velox Server Properties configured'
      logit INFO '-------------------------------------------'	  
    else
      logit INFO '-------------------------------------------'	
      logit INFO 'Velox Server Properties configured failed'
      logit INFO '-------------------------------------------'	  
	  exit 1
    fi
  fi	
  
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting DatamgmtServer'
  logit INFO '-------------------------------------------'	  
  run_DatamgmtServer 
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Started'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'DataMgmt Server Failed'
    logit INFO '-------------------------------------------'	  
  fi

  if [ -f $STAGE_DIR/"$EXEMPLAR_LIMS_DELIVERY.zip" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Applying LIMS Delivery Patch'
    logit INFO '-------------------------------------------'	  
    apply_lims_delivery 
    rc=$?  
    if [ $rc = "0" ]; then
      logit INFO '-------------------------------------------'	
      logit INFO 'LIMS Delivery Patch Applied'
      logit INFO '-------------------------------------------'	  
    else
      logit INFO '-------------------------------------------'	
      logit INFO 'LIMS Delivery Patch Failed'
      logit INFO '-------------------------------------------'	  
	  exit 1
    fi
  fi	
  logit INFO '-------------------------------------------'	
  logit INFO 'BLS App Components Installed'
  logit INFO '-------------------------------------------'	
}