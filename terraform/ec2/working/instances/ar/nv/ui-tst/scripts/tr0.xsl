<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="Service">
    <Service>
      <Connector port="8443" protocol="org.apache.coyote.http11.Http11Nio2Protocol" maxThreads="150" SSLEnabled="true" scheme="https" secure="true" compression="on" compressionMinSize="2048" compressableMimeType="text/html,text/xml,text/javascript,application/x-javascript,application/javascript,text/css" useSendfile="false">
        <UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol"/>
      </Connector>
      <xsl:apply-templates/>
    </Service>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="Connector">
    <Connector>
      <SSLHostConfig>
        <Certificate certificateKeystoreFile="/opt/tomcat/ssl-certs/.keystore" certificateKeystorePassword="AbbVie2013" certificateKeyAlias="tomcat"/>
      </SSLHostConfig>
      <xsl:apply-templates/>
    </Connector>
  </xsl:template>
</xsl:stylesheet>
