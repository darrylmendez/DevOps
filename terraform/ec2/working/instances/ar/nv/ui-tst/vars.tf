variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "SSL_PASSWORD" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair-ui-tst"
}

variable "path_to_public_key" {
  default = "rlimskeypair-ui-tst.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

# This is ui base AMI
variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
  }
}

variable "guid" {
  default = "813a36c9-c0c7-4eb7-8261-2c12f03826cc"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.large"
}

variable "exemplar_lims_server_ip" {
  default = "10.239.11.49"
}

variable "email_host_ip" {
  default = "15.163.124.148"
}

variable "primary_eni_id" {
  default = "eni-911a6fc4"
}

variable "root_volume_size" {
  default = "250"
}

variable "exemplar_base" {
  default = "exemplarlims_v1060_b770.zip"
}
