#Define the init.cfg commands,
#add specific commands for adding user, groups etc in init.cfg
data "template_file" "init-script-ui" {
  template = "${file("scripts/init-ui.cfg")}"

  vars {
    REGION = "${var.aws_region}"
  }
}

data "template_file" "server-xml-ui" {
  template = "${file("scripts/server.xml")}"

  vars {
    REGION = "${var.SSL_PASSWORD}"
  }
}

data "template_cloudinit_config" "cloudinit-rlims-ui" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init-ui.cfg"
    content_type = "text/cloud-config"
    content      = "${data.template_file.init-script-ui.rendered}"
  }
  
   part {
    filename     = "server-xml-ui"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.server-xml-ui.rendered}"
  }
}
