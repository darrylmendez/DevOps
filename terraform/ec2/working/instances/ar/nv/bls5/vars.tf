variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable "az" {
  default = "us-east-1a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-Dev"
}

variable "path_to_private_key" {
  default = "rlimskeypair"
}

variable "path_to_public_key" {
  default = "rlimskeypair.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "instance_device_name" {
  default = "/dev/xvdh"
}

variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-010bd55b40be4e7e6"
  }
}

variable "guid" {
  default = "813a36c9-c0c7-4eb7-8261-2c12f03826cc"
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}

variable "private_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = "subnet-205f5508"
}

variable "public_availability_zones" {
  description = "List of potential spots for instances within a region"
  type        = "list"

  default = [
    "us-east-1a",
    "us-east-1b",
    "us-east-1c",
  ]
}

variable "ebs_volume_size" {
  default = "100"
}

variable "secondary_eni_id" {
  default = "eni-61cd2675"
}

variable "security_group_ids" {
  description = "allow-ssh"
  type        = "list"

  default = [
    "sg-1fa8e954",
  ]
}
