variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "aws_region" {
  default = "us-east-1"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "DevOps EC2"
}

variable "path_to_private_key" {
  default = "devops"
}

variable "path_to_public_key" {
  default = "devops.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

variable "amis" {
  type = "map"

  default = {
    us-east-1 = "ami-6871a115"
  }
}

variable "vpc_id" {
  description = "Target VPC to launch in"
  default     = "vpc-8df951e8"
}

variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.micro"
}

variable "public_subnet" {
  description = "Available private subnet ids for availability zones"
  default     = "subnet-205f5508"
}

variable "private_subnets" {
  description = "Available private subnet ids for availability zones"
  type        = "list"

  default = [
    "subnet-205f5508",
  ]
}



