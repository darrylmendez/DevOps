#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/deployment
export WS_CONFIG_FILE_NAME=$TOMCAT_APP_INSTALL/velox_webservice/config/VeloxWebServices.properties
export EXEMPLAR_LIMS_SERVER_IP=$1
export EMAIL_SENDER=rlims.support@abbvie.com
export PROGRAM_NAME=$(basename $0)
export tmpfile=/tmp/prop.$$
export TOMCAT_DIR=/opt/tomcat
#export commonname=`ip route get 1 | awk '{print $NF;exit}'`
export commonname=$2
export SWAGGER_JSON=$TOMCAT_APP_INSTALL/velox_webservice/docs/swagger.json
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
wait_for_file() {
  local ctr=0
  while [ ! -f $1 ]
  do
    sleep 1
	ctr=`expr $ctr + 1`
	if [ $ctr -eq 600 ];	then
	  logit ERROR "Waited too long for $1, aborting process"
	fi
 done
}
unzip_file() {
  sudo unzip "$1" -d "$2" 
}
change_group() {
  sudo chgrp $1 $2 || logit ERROR "Error changing group $1 $2"
}
change_owner() {
  sudo chown -Rf $1 $2 || logit ERROR "Error changing owner $1 $2"
}
change_owner_group() {
  sudo chown $1:$2 $3|| logit ERROR "sudo chown $1:$2 $3"
}  
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
}

config_ws_json() {
 sudo sed -i -- "s/localhost:8443/$commonname/g" $SWAGGER_JSON
 grep host $SWAGGER_JSON
}
configure_ws_props() {
 logit INFO "Waiting for $WS_CONFIG_FILE_NAME"
 wait_for_file $WS_CONFIG_FILE_NAME 
 logit INFO "Found $WS_CONFIG_FILE_NAME"
 logit INFO "Configuring $WS_CONFIG_FILE_NAME - exemplarlims.server.ip - $EXEMPLAR_LIMS_SERVER_IP"
 configure_props $WS_CONFIG_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 grep exemplarlims.server.ip $WS_CONFIG_FILE_NAME
 config_ws_json
}
deploy_ws() {
  logit INFO "Configuring Web Services"
  logit INFO "Copy $STAGE_DIR/velox_webservice.war to $TOMCAT_APP_INSTALL/"
  sudo cp $STAGE_DIR/velox_webservice.war $TOMCAT_APP_INSTALL/ || logit ERROR "Error Copying velox_webservice.war to $TOMCAT_APP_INSTALL"
  logit INFO "Configuring Web Service Properties"
  configure_ws_props
  #sudo systemctl stop tomcat
  #sudo systemctl start tomcat
  grep exemplarlims.server.ip $WS_CONFIG_FILE_NAME
  grep host $SWAGGER_JSON
}

{
  ec=0
 
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Configuration of Web Service  '
  logit INFO '-------------------------------------------'
  deploy_ws
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Complete'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Change ownership of tomcat '
  logit INFO '-------------------------------------------'
  change_owner_group tomcat tomcat /opt/tomcat
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Changed ownership of tomcat'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Failure to change tomcat ownerhip'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
