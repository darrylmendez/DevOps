#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 09/21/2018
# Description	: Initial Revision

export password=$1 
export commonname=`ip route get 1 | awk '{print $NF;exit}'`
export dns=$2
export country=US
export state=IL
export locality="North Chicago"
export organization="AbbVie Inc."
export organizationalunit=BTS
export email=darryl.mendez@abbvie.com
export TOMCAT_DIR=/opt/tomcat
export TOMCAT_CONF=$TOMCAT_DIR/conf
export TC_CONF_FILE=$TOMCAT_CONF/server.xml
export STAGE_DIR=/tmp/
export SCRIPTS_DIR=$STAGE_DIR/scripts
export XSL_FILE=$SCRIPTS_DIR/transform.xsl

error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
}
configure_ssl_props() {
  sudo cp $TC_CONF_FILE $TC_CONF_FILE.old
  sudo xsltproc -o $TC_CONF_FILE $XSL_FILE $TC_CONF_FILE 
  sudo sed -i -- "s/"DefaultPassword"/$password/g" $TC_CONF_FILE
}
csr() { 
  cd $TOMCAT_DIR
  sudo mkdir ssl-certs
  cd $TOMCAT_DIR/ssl-certs
  echo "Generating key request for $domain"
  #Generate a key
  openssl genrsa -out rlims-private.pem 2048 
  #Create the request
  echo "Creating CSR"
  openssl req -new -x509 -key rlims-private.pem -out rlims-cert.pem -passin pass:$password \
    -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
  openssl pkcs12 -export -out rlims-keystore.pkcs12 -in rlims-cert.pem -inkey rlims-private.pem -passout pass:$password 
  echo $password | keytool -importkeystore -srckeystore rlims-keystore.pkcs12 -srcstoretype PKCS12 -srcalias 1 -destkeystore .keystore -deststoretype JKS -destalias tomcat -storepass $password
  #Load balancer CSR
  openssl req -nodes -newkey rsa:2048 -keyout rlims-ssl.key -out rlims.csr -subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$dns/emailAddress=$email"
  sudo chown tomcat:tomcat $TOMCAT_DIR/ssl-certs
}

{
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Certificate Installation'
  logit INFO '-------------------------------------------'
  logit INFO '-------------------------------------------'
  logit INFO 'Generate ssl certs'
  logit INFO '-------------------------------------------'
  csr
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO "Certificate install successful"
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO 'Certificate install Failed'
    logit INFO '-------------------------------------------'
	exit 1
  fi

  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting SSL configuration '
  logit INFO '-------------------------------------------'
  configure_ssl_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'SSL configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'SSL configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
   
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'CSR Installation/Conf Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
