resource "aws_instance" "rlims-devops-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the VPC subnet
  subnet_id = "${var.public_subnet}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.devops-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.devops.key_name}"



  tags = {
    Name        = "${var.customer}-${var.environment}-devops"
    environment = "${var.environment}"
  }


  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }
}


