#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
# Define Variables
export PROGRAM_NAME=`basename "$0"`
export bartender_ip=$1
export bartender_account=$2
export bartender_password=$3
export tmpfile=/tmp/prop.$$
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
#configure key pair values 
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
} 
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}
mnt_bartender() {
  cd /mnt
  if [ ! -d /mnt/bartender ]; then
    sudo mkdir /mnt/bartender
  fi
  sudo yum install -y nfs-utils
  sudo yum install -y cifs-utils
  sudo chown rlimsadmin:rlimsadmin /mnt/bartender
  #reusing Phase 1 service account
  #sudo echo "//$bartender_ip/bartender/rlims/data/inbound /mnt/bartender cifs username=$bartender_account,password=$bartender_password,uid=rlimsadmin,gid=rlimsadmin,defaults 0 0" >> /etc/fstab
  sudo echo "//$bartender_ip/bartender/rlims/data /mnt/bartender cifs username=$bartender_account,password=$bartender_password,uid=rlimsadmin,gid=rlimsadmin,defaults 0 0" >> /etc/fstab
 
  sudo mount -a || logit ERROR "sudo mount -a"
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  ec=0
  logit INFO '-------------------------------------------'	
  logit INFO 'Mount Bartender'
  logit INFO '-------------------------------------------'	  
  mnt_bartender
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Bartender Mounted'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'Bartender Mount Failed'
    logit INFO '-------------------------------------------'	  
  fi
  logit INFO '-------------------------------------------'	
  logit INFO 'Bartender Mounted'
  logit INFO '-------------------------------------------'	
  
  
}