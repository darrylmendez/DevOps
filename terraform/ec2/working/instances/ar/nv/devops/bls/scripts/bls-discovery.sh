#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 10/01/2018
# Description	: Initial Revision
# Define Variables
export DISCOVERY_EXTRACTOR_ZIP=$1
export DISCOVERY_EXTRACTOR_NAME=$2
export DISCOVERY_INSTALL_PATH=$3
export DISCOVERY_GUID=$4
export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export PROGRAM_NAME=`basename "$0"`
export STAGE_DIR=/tmp/deployment
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
}
wait_for_dir() {
  local ctr=0
  while [ ! -d "$1" ]
  do
    sleep 1
	ctr=`expr $ctr + 1`
	if [ $ctr -eq 600 ];	then
	  logit ERROR "Waited too long for $1, aborting process"
	fi
 done
}
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}

deploy_discovery() {
 cd $STAGE_DIR
 wait_for_dir $DISCOVERY_INSTALL_PATH 
 sudo cp $STAGE_DIR/"$DISCOVERY_EXTRACTOR_ZIP" $DISCOVERY_INSTALL_PATH || logit ERROR "Unable to copy files from $STAGE_DIR/discovery_extractor to $DISCOVERY_INSTALL_PATH"
 cd $DISCOVERY_INSTALL_PATH
 unzip_file "$DISCOVERY_EXTRACTOR_ZIP" 
 sudo chown -R rlimsadmin:rlimsadmin $EXEMPLAR_INSTALL_DIR
 sudo -u rlimsadmin java -Dappguid=$DISCOVERY_GUID -jar $DISCOVERY_INSTALL_PATH/$DISCOVERY_EXTRACTOR_NAME -donotprompt
}

# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set +e
  ec=0
  if [ ! -z "$DISCOVERY_GUID" ]; then
    logit INFO '------------------------------------------------'
    logit INFO 'Starting BLS Server Extractor Discovery Install'
    logit INFO '------------------------------------------------'
    logit INFO '------------------------------------------------'
    logit INFO 'Deploy Discovery'
    logit INFO '------------------------------------------------'
    deploy_discovery
    rc=$?
    if [ $rc = "0" ]; then
      logit INFO '-----------------------------------------------'
      logit INFO 'Deployment of Extractor Discovery successful'
      logit INFO '-----------------------------------------------'
    else
  	  ec=1
      logit INFO '-----------------------------------------------'
      logit INFO "Deployment of Extractor Discovery Failed"
      logit INFO '-----------------------------------------------'
	  exit 1
    fi
  fi

  
}