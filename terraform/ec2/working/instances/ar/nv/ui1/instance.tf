resource "aws_instance" "rlims-ui-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the VPC subnet
  subnet_id = "subnet-205f5508"

  # the security group
  #vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]
  vpc_security_group_ids = "${var.security_group_ids}"

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-ui.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads/common"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-UI"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/ui.sh",
      "sudo /tmp/scripts/ui.sh ${var.guid} ${var.exemplar_lims_server_ip} ${var.email_host_ip}",
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-ui-instance.private_ip} >> ui-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }

  network_interface {
    device_index          = 0
    interface_id          = "${var.secondary_eni_id}"
    delete_on_termination = false
  }


}

#resource "aws_network_interface_attachment" "DevSci-RLIMS-UI" {
#  instance_id          = "${aws_instance.rlims-ui-instance.id}"
#  network_interface_id = "${var.secondary_eni_id}"
#  device_index         = 0
#}

resource "aws_ebs_volume" "ebs-volume-1" {
  availability_zone = "${var.az}"
  size              = "${var.ebs_volume_size}"
  type              = "gp2"

  tags {
    Name = "extra volume data"
  }
}

resource "aws_volume_attachment" "ebs-volume-1-attachment" {
  device_name = "${var.instance_device_name}"
  volume_id   = "${aws_ebs_volume.ebs-volume-1.id}"
  instance_id = "${aws_instance.rlims-ui-instance.id}"
}
