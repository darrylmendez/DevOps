#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/common
export EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/veloxClient/config/ExemplarWebClient.properties
export PORTAL_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/velox_portal/config/PortalClient.properties
export WS_CONFIG_FILE_NAME=$TOMCAT_APP_INSTALL/velox_webservice/config/VeloxWebServices.properties
export GUID=$1
export EXEMPLAR_LIMS_SERVER_IP=$2
export EMAIL_HOST_IP=$3
export EMAIL_SENDER=rlims.support@abbvie.com
export PROGRAM_NAME=$(basename $0)
export EXEMPLAR_WS_EXTRACTOR=exemplar_9.6.1_extractor.jar
export tmpfile=/tmp/prop.$$
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }

move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
start_tc() {
  sudo /bin/su -p -s /bin/sh tomcat /opt/tomcat/bin/startup.sh || logit ERROR "Error Starting Tomcat" 
}
abbvie_delivery_extractor() {
  sudo java -Dappguid=$GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR || logit ERROR "Error running Delivery Extractor"
}
deploy_ws() {
  sudo java -Dappguid=$GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$EXEMPLAR_WS_EXTRACTOR -donotprompt || logit ERROR "Error Deploying Web Service"
}
change_group() {
  sudo chgrp $1 $2 || logit ERROR "Error changing group $1 $2"
}
change_owner() {
  sudo chown $1 $2 || logit ERROR "Error changing owner $1 $2"
  }
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
}
configure_web_client_props() {
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email.host $EMAIL_HOST_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email_sender $EMAIL_SENDER
 change_group tomcat $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME
 change_owner tomcat $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME
}
configure_portal_client_props() {
 configure_props $PORTAL_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 change_group tomcat $PORTAL_CLIENT_PROP_FILE_NAME
 change_owner tomcat $PORTAL_CLIENT_PROP_FILE_NAME
}
configure_ws_props() {
 configure_props $WS_CONFIG_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 change_group tomcat $WS_CONFIG_FILE_NAME
 change_owner tomcat $WS_CONFIG_FILE_NAME
}
{
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server App Install'
  logit INFO '-------------------------------------------'
  
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Tomcat '
  logit INFO '-------------------------------------------'
  start_tc
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Tomcat Started'
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO 'Tomcat not successfully started'
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting abbvie_delivery_extractor '
  logit INFO '-------------------------------------------'
  abbvie_delivery_extractor
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'AbbVie Delivery Extractor completed'
    logit INFO '-------------------------------------------'
  else
  	ec=1
	logit INFO '-------------------------------------------'
    logit INFO 'Failure - AbbVie Delivery Extractor '
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Exemplar Web Service Deployment'
  logit INFO '-------------------------------------------'
  deploy_ws
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Exemplar Web Service Deployment Successful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Exemplar Web Service Deployment Failed'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  sleep 120
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Web Client Props'
  logit INFO '-------------------------------------------'
  configure_web_client_props
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Portal Client '
  logit INFO '-------------------------------------------'

  configure_portal_client_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Web Service  '
  logit INFO '-------------------------------------------'

  configure_ws_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Complete'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Complete Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'UI Server Installation/Conf Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
