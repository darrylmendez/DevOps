#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
# Define Variables
export PROGRAM_NAME=`basename "$0"`
export EXEMPLAR_INSTALL_DIR=/opt/rlims/
export STAGE_DIR=/tmp/deployment
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
#configure key pair values 
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
} 
#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip -o "$1" -d "$2"
}
#Verify Directory contents
verify_directory() {
  if [ ! -d $1 ]; then
    exit 1
  fi 
}
update_profile() {
  echo "export LD_LIBRARY_PATH=/usr/lib/oracle/18.3/client64/lib:$LD_LIBRARY_PATH" >> ~/.bash_profile
  echo "export PATH=/usr/lib/oracle/18.3/client64/bin:$PATH" >> ~/.bash_profile
  source ~/.bash_profile
}
install_oracle_rpm() {
  cd $STAGE_DIR
  sudo rpm -ivh oracle-instantclient18.3-basic-18.3.0.0.0-1.x86_64.rpm
  sudo rpm -ivh oracle-instantclient18.3-sqlplus-18.3.0.0.0-1.x86_64.rpm
  sudo rpm -ivh oracle-instantclient18.3-tools-18.3.0.0.0-1.x86_64.rpm
  update_profile
}  
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  ec=0
  logit INFO '-------------------------------------------'	
  logit INFO 'Install Oracle Client'
  logit INFO '-------------------------------------------'	  
  install_oracle_rpm
  rc=$?  
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Oracle Client installed'
    logit INFO '-------------------------------------------'	  
  else
    logit INFO '-------------------------------------------'	
    logit INFO 'Oracle Client install Failed'
    logit INFO '-------------------------------------------'	  
  fi
  logit INFO '-------------------------------------------'	
  logit INFO 'BLS App Components Installed'
  logit INFO '-------------------------------------------'	
  
  
}