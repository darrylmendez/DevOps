#!/bin/bash
# RLIMS BLS Base Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export LIBREOFFICE=LibreOffice_5.4.6_Linux_x86-64_rpm.tar.gz
export STAGE_DIR=/tmp/deployment
export JDK_RPM=jdk-8u181-linux-x64.rpm
export MARS_HOST=$1
#exit function
error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
upgrade() {
  logit INFO 'Starting Package Update'
  sudo yum update -y || logit ERROR "sudo yum update - y"
  sudo yum upgrade -y || logit ERROR "sudo yum upgrade -y"
  logit INFO 'Package Successfully Updated'
}
add_mars_host() {
    sudo echo "$MARS_HOST" >> /etc/hosts || logit ERROR "Unable to add MARS Host $MARS_HOST >> /etc/hosts"
}
install_utils() {
 #Perform Update
  logit INFO 'Starting Utils Install'
  sudo yum install -y unzip wget zip vim libaio openssl || logit ERROR "sudo yum install -y unzip wget zip vim libaio openssl"
  logit INFO 'Completed Utils Installation'

}
install_libre_office() {
  logit INFO 'Starting Libreoffice Install'
  cd $STAGE_DIR
  tar -zxvf $LIBREOFFICE || logit ERROR "tar -zxvf $LIBREOFFICE_RPM"
  cd LibreOffice*/RPMS || logit ERROR "cd LibreOffice*/RPMS"
  sudo yum install -y *.rpm
  sudo yum install -y cairo cups-libs libSM
  logit INFO 'Libre Office Successfully Installed'
}
add_rlims_user() {
  logit INFO 'Starting rlimsadmin user and group creation'
  sudo groupadd rlimsadmin || logit ERROR "sudo groupadd rlimsadmin "
  sudo useradd -s /bin/bash -g rlimsadmin -d /opt/rlims rlimsadmin || logit ERROR "sudo useradd -s /bin/bash -g rlimsadmin -d /opt/rlims rlimsadmin "
  logit INFO 'rlimadmin user and group created'
}

install_jdk() {
  sudo rpm -ivh $STAGE_DIR/$JDK_RPM  || logit ERROR " sudo rpm -ivh $STAGE_DIR/$JDK_RPM "
}
cleanup() {
  cd $STAGE_DIR
  #sudo rm $LIBREOFFICE

}
{
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting BLS Base Install'
  logit INFO '-------------------------------------------'	
  logit INFO 'Starting Package update/upgrade'
  logit INFO '...................................'
  ec=0
  upgrade
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed Package update/upgrade'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-------------------------------------------'	
    logit INFO 'Package update/upgrade Failed'
    logit INFO '-------------------------------------------'
    exit 1
  fi 
  logit INFO 'Starting Utils Install'
  logit INFO '...................................'  
  install_utils
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed Utils Install'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed Utils Install Failed'
    logit INFO '-------------------------------------------'	
    exit 1
  fi 
  logit INFO 'Starting Libreoffice Install'
  logit INFO '...................................'  
  install_libre_office
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed Libreoffice Install'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-------------------------------------------'	
    logit INFO 'Libreoffice Install Failed'
    logit INFO '-------------------------------------------'	
    exit 1
  fi 
  logit INFO 'Creating User/Group'
  logit INFO '...................................'    
  add_rlims_user
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed User/Group creation'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed User/Group creation Failed'
    logit INFO '-------------------------------------------'	
    exit 1
  fi 
  logit INFO 'Starting JDK Install'
  logit INFO '...................................'    
  install_jdk
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'	
    logit INFO 'Completed JDK Install'
    logit INFO '-------------------------------------------'	
  else
    ec=1
    logit INFO '-------------------------------------------'	
    logit INFO 'JDK Install Failed'
    logit INFO '-------------------------------------------'	
    exit 1
  fi 
  
  
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------------'
    logit INFO 'BLS Base installation and configuration complete'
    logit INFO '-------------------------------------------------'
	exit 0
  fi
}
