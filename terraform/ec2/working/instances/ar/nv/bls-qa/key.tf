resource "aws_key_pair" "rlimskeypair" {
  key_name   = "rlimskeypair-bls-qa"
  public_key = "${file("${var.path_to_public_key}")}"
}
