#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export EXEMPLAR_BASE=$1
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/deployment
export EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/veloxClient/config/ExemplarWebClient.properties
export PORTAL_CLIENT_PROP_FILE_NAME=$TOMCAT_APP_INSTALL/velox_portal/config/PortalClient.properties
export WS_CONFIG_FILE_NAME=$TOMCAT_APP_INSTALL/velox_webservice/config/VeloxWebServices.properties
export EXEMPLAR_LIMS_SERVER_IP=$2
export EMAIL_HOST_IP=$3
export EMAIL_SENDER=rlims.support@abbvie.com
export PROGRAM_NAME=$(basename $0)
export tmpfile=/tmp/prop.$$

error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" 
}
deploy_lims_jars() {
  unzip_file $STAGE_DIR/"$EXEMPLAR_BASE" $STAGE_DIR
  sudo cp $STAGE_DIR/veloxClient/veloxClient.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/veloxclient/veloxClient.war to $TOMCAT_APP_INSTALL"
  sudo cp $STAGE_DIR/velox_portal/velox_portal.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/velox_portal/velox_portal.war to $TOMCAT_APP_INSTALL"
  sudo chown tomcat:tomcat $TOMCAT_APP_INSTALL 
}
install_certificates() {
  #Create Private Key
  openssl genrsa -out rlims-private.pem 2048
  #Create Self Signed Certificate
  openssl req -new -x509 -key rlims-private.pem -out rlims-cert.pem -days 3650
  #Create the PKCS12 keystore from the X509 certificate 
  openssl pkcs12 -export -out rlims-keystore.pkcs12 -in rlims-cert.pem -inkey rlims-private.pem
  #Import the certificate to Java Key Store
  $JAVA_HOME/keytool -importkeystore -srckeystore rlims-keystore.pkcs12 -srcstoretype PKCS12 -srcalias 1 -destkeystore .keystore -deststoretype JKS -destalias tomcat


}
start_tc() {
  logit INFO "sudo systemctl daemon-reload"
  sudo systemctl daemon-reload || logit ERROR "sudo systemctl daemon-reload"
  logit INFO "sudo systemctl enable tomcat"
  sudo systemctl enable tomcat || logit ERROR "sudo systemctl enable tomcat"
  logit INFO "sudo systemctl start tomcat"
  sudo systemctl start tomcat
  logit INFO "sudo systemctl status tomcat"
  sudo systemctl status tomcat
  #source /home/ec2-user/.bash_profile
  #sudo /bin/su -p -s /bin/sh tomcat /opt/tomcat/bin/startup.sh || logit ERROR "Error Starting Tomcat" 
}
start_tc2() {
  source /home/ec2-user/.bash_profile
  sudo /bin/su -p -s /bin/sh tomcat /opt/tomcat/bin/startup.sh || logit ERROR "Error Starting Tomcat" 
}
change_group() {
  sudo chgrp $1 $2 || logit ERROR "Error changing group $1 $2"
}
change_owner() {
  sudo chown -Rf $1 $2 || logit ERROR "Error changing owner $1 $2"
  }
change_owner_group() {
  sudo chown $1:$2 $3|| logit ERROR "sudo chown $1:$2 $3"
}  
configure_props() {
  local file_name=$1
  local prop_name=$2
  local prop_value=$3
  sudo sed "/^$prop_name/s/=.*$/=$prop_value/" $file_name > $tmpfile || logit ERROR "Error configuring $file_name, $prop_name with $prop_value"
  sudo mv -f $tmpfile $file_name
}
configure_web_client_props() {
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email.host $EMAIL_HOST_IP
 configure_props $EXEMPLAR_WEB_CLIENT_PROP_FILE_NAME email_sender $EMAIL_SENDER
 sudo chown tomcat:tomcat /opt/tomcat
}
configure_portal_client_props() {
 configure_props $PORTAL_CLIENT_PROP_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
}
configure_ws_props() {
 configure_props $WS_CONFIG_FILE_NAME exemplarlims.server.ip $EXEMPLAR_LIMS_SERVER_IP
 sudo chown tomcat:tomcat /opt/tomcat
}
{
  sleep 1
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server App Install'
  logit INFO '-------------------------------------------'
  logit INFO '-------------------------------------------'
  logit INFO 'Deploy LIMS JARS'
  logit INFO '-------------------------------------------'
  deploy_lims_jars
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO "Deployment of LIMS JARS successful"
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO "Deployment of LIMS JARS Failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting Tomcat '
  logit INFO '-------------------------------------------'
  start_tc
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Tomcat Started'
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO 'Tomcat not successfully started'
    logit INFO '-------------------------------------------'
	exit 1
  fi
  sleep 60
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Web Client Props'
  logit INFO '-------------------------------------------'
  configure_web_client_props
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Client Props Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Portal Client '
  logit INFO '-------------------------------------------'
  configure_portal_client_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Sucessful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Portal Client Configuration Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Starting configuration of Web Service  '
  logit INFO '-------------------------------------------'

  #configure_ws_props
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Complete'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Web Service Configuration Complete Failure'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  logit INFO ''
  logit INFO '-------------------------------------------'
  logit INFO 'Change ownership of tomcat '
  logit INFO '-------------------------------------------'
  change_owner_group tomcat tomcat /opt/tomcat
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Changed ownership of tomcat'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Failure to change tomcat ownerhip'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  
  if [ $ec = "0" ]; then  
    logit INFO ''
    logit INFO '-------------------------------------------'
    logit INFO 'UI Server Installation/Conf Completed.'
    logit INFO '-------------------------------------------'
	exit 0
  fi
}
