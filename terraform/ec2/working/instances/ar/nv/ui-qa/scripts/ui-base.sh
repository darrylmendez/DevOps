#!/bin/bash
# RLIMS UI/Tomcat Server Base Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export TOMCAT_ZIP=apache-tomcat-9.0.10.tar.gz
export STAGE_DIR=/tmp/deployment
export INSTALL_DIR=/opt
export TOMCAT_DIR=$INSTALL_DIR/tomcat
export JDK=jdk-8u181-linux-x64.tar.gz
export JDK_RPM=jdk-8u181-linux-x64.rpm

error_exit()
{
  echo "$1" 1>&2
  exit 1
}
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
  if [ ${LOG_LEVEL} = "ERROR" ]; then
    error_exit $MSG
  fi
 }
upgrade() {
  logit INFO 'Starting Package Update'
  sudo yum update -y || logit ERROR "sudo yum update - y"
  sudo yum upgrade -y || logit ERROR "sudo yum upgrade -y"
  logit INFO 'Package Successfully Updated'
} 
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
create_group() {
  sudo groupadd $1 || logit ERROR "Error creating group $1"
}
create_user() {
  sudo useradd -M -s /bin/nologin -g $1 -d $2 $3 || logit ERROR "Error creating user $1 $2 $3"
}
install_jdk_rpm() {
  logit INFO 'Installing JDK'
  sudo rpm -ivh $STAGE_DIR/$JDK || logit ERROR "sudo rpm -ivh $JDK"
}
install_tomcat() {
  logit INFO "Creating tomcat Directory : $TOMCAT_DIR"
  sudo mkdir $TOMCAT_DIR
  cd $TOMCAT_DIR
  logit INFO "Extracting $TOMCAT_ZIP from $STAGE_DIR to $TOMCAT_DIR"
  sudo tar xvzf $STAGE_DIR/$TOMCAT_ZIP -C $TOMCAT_DIR --strip-components=1 
  sudo chgrp -R tomcat conf
  sudo chmod g+rwx conf
  sudo chmod g+r conf/*
  sudo chown -R tomcat logs/ temp/ webapps/ work/
  sudo chgrp -R tomcat bin
  sudo chgrp -R tomcat lib
  sudo chmod g+rwx bin
  sudo chmod g+r bin/*
  sudo chown -Rf tomcat:tomcat $TOMCAT_DIR || logit ERROR "sudo chown -Rf tomcat:tomcat $TOMCAT_DIR"
  sudo mv /tmp/scripts/tomcat.service /etc/systemd/system/
  sudo chmod 644 /etc/systemd/system/tomcat.service
  sudo chown root:root /etc/systemd/system/tomcat.service
  
  logit INFO "sudo systemctl daemon-reload"
  sudo systemctl daemon-reload || logit ERROR "sudo systemctl daemon-reload"
  logit INFO "sudo systemctl enable tomcat"
  sudo systemctl enable tomcat || logit ERROR "sudo systemctl enable tomcat"
  logit INFO "sudo systemctl start tomcat"
  sudo systemctl start tomcat
  logit INFO "sudo systemctl status tomcat"
  sudo systemctl status tomcat

}
install_utils() {
  sudo yum install -y unzip   || logit ERROR "Error installing Utilities - unzip "
  sudo yum install -y wget    || logit ERROR "Error installing Utilities - wget "
  sudo yum install -y zip     || logit ERROR "Error installing Utilities - zip "
  sudo yum install -y vim     || logit ERROR "Error installing Utilities - vim "
  sudo yum install -y libaio  || logit ERROR "Error installing Utilities - libaio"
  sudo yum install -y openssl || logit ERROR "Error installing Utilities - openssl"
}
install_jdk_rpm() {
  logit INFO 'Installing JDK'
  sudo rpm -ivh $STAGE_DIR/$JDK_RPM || logit ERROR "sudo rpm -ivh $JDK_RPM"
}
{
  ec=0
  logit INFO '-----------------------------------------------'
  logit INFO 'Starting UI Server Base Install'
  logit INFO '-----------------------------------------------'

  logit INFO '-----------------------------------------------'
  logit INFO 'Package Upgrade'
  logit INFO '-----------------------------------------------'
  upgrade
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'Package Upgraded'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'Package Upgrade failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi   
 

  logit INFO '-----------------------------------------------'
  logit INFO 'Installing Utils'
  logit INFO '-----------------------------------------------'
  install_utils
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'UTILS Installed'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'UTILS Installation failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi 
  logit INFO '-----------------------------------------------'
  logit INFO 'Creating tomcat group'
  logit INFO '-----------------------------------------------'
  create_group tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat group created'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat group creation failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  logit INFO '-----------------------------------------------'
  logit INFO 'Creating tomcat user'
  logit INFO '-----------------------------------------------'
  create_user tomcat $TOMCAT_DIR tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'Created tomcat user'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat user failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  logit INFO '-----------------------------------------------'
  logit INFO 'Installing JDK '
  logit INFO '-----------------------------------------------'
  install_jdk_rpm
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'JDK Sucessfully Installed'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'JDK Install Failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  logit INFO '-----------------------------------------------'
  logit INFO 'Installing tomcat '
  logit INFO '-----------------------------------------------'
  install_tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'Installed tomcat'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat installation failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  exit 0
  
  if [ $ec = "0" ]; then  
    logit INFO '-----------------------------------------------'
    logit INFO 'Completed UI Base Server Install'
    logit INFO '-----------------------------------------------'
	exit 0
  fi  
}
