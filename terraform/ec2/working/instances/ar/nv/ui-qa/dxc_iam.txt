#DXC MSP
data "template_file" "ec2_instance_profile_DXC-CloudWatchEventsPolicy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/DXC-CloudWatchEventsPolicy.json.template")}"
}

data "template_file" "ec2_instance_profile_DXC-CloudWatchLogsPolicy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/DXC-CloudWatchLogsPolicy.json.template")}"
}

data "template_file" "ec2_instance_profile_DXC-MSPPolicy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/DXC-MSPPolicy.json.template")}"
}

data "template_file" "ec2_instance_profile_DXC-WindowsPatchingPolicy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/DXC-WindowsPatchingPolicy.json.template")}"
}

data "template_file" "ec2_instance_profile_DXC-LinuxPatchingPolicy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/DXC-LinuxPatchingPolicy.json.template")}"
}

data "template_file" "ec2_instance_profile_assume_role_policy" {
  template = "${file("${path.module}/templates/policies/instance_profile/DXC_MSP_part/assume_role_policy.json.template")}"
}

resource "aws_iam_role" "instance_profile" {
  name               = "ec2-instance-profile-role-DXCMSP-DevSci"
  assume_role_policy = "${data.template_file.ec2_instance_profile_assume_role_policy.rendered}"
}

resource "aws_iam_instance_profile" "instance_profile" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-DXCMSP-DevSci"
  role       = "${aws_iam_role.instance_profile.id}"
}

resource "aws_iam_policy" "instance_profile_Windowspatching" {
  name   = "ec2-instance-profile-Windowspatching"
  policy = "${data.template_file.ec2_instance_profile_DXC-WindowsPatchingPolicy.rendered}"
}

resource "aws_iam_policy_attachment" "instance_profile_Windowspatching" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-Windowspatching-att"
  policy_arn = "${aws_iam_policy.instance_profile_Windowspatching.id}"
  roles      = ["${aws_iam_role.instance_profile.id}"]
}

resource "aws_iam_policy" "instance_profile_Linuxpatching" {
  name   = "ec2-instance-profile-Linuxpatching"
  policy = "${data.template_file.ec2_instance_profile_DXC-LinuxPatchingPolicy.rendered}"
}

resource "aws_iam_policy_attachment" "instance_profile_Linuxpatching" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-Linuxpatching-att"
  policy_arn = "${aws_iam_policy.instance_profile_Linuxpatching.id}"
  roles      = ["${aws_iam_role.instance_profile.id}"]
}

resource "aws_iam_policy" "instance_profile_CloudWatchEventsPolicy" {
  name   = "ec2-instance-profile-CloudWatchEventsPolicy"
  policy = "${data.template_file.ec2_instance_profile_DXC-CloudWatchEventsPolicy.rendered}"
}

resource "aws_iam_policy_attachment" "instance_profile_CloudWatchEventsPolicy" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-CloudWatchEventsPolicy-att"
  policy_arn = "${aws_iam_policy.instance_profile_CloudWatchEventsPolicy.id}"
  roles      = ["${aws_iam_role.instance_profile.id}"]
}

resource "aws_iam_policy" "instance_profile_CloudWatchLogsPolicy" {
  name   = "ec2-instance-profile-CloudWatchLogsPolicy"
  policy = "${data.template_file.ec2_instance_profile_DXC-CloudWatchLogsPolicy.rendered}"
}

resource "aws_iam_policy_attachment" "instance_profile_CloudWatchLogsPolicy" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-CloudWatchLogsPolicy-att"
  policy_arn = "${aws_iam_policy.instance_profile_CloudWatchLogsPolicy.id}"
  roles      = ["${aws_iam_role.instance_profile.id}"]
}

resource "aws_iam_policy" "instance_profile_MSPPolicy" {
  name   = "ec2-instance-profile-MSPPolicy"
  policy = "${data.template_file.ec2_instance_profile_DXC-MSPPolicy.rendered}"
}

resource "aws_iam_policy_attachment" "instance_profile_MSPPolicy" {
  depends_on = ["aws_iam_role.instance_profile"]
  name       = "ec2-instance-profile-MSPPolicy-att"
  policy_arn = "${aws_iam_policy.instance_profile_MSPPolicy.id}"
  roles      = ["${aws_iam_role.instance_profile.id}"]
}

output "ec2-instance-profile-DXCMSP-DevSci_id" {
  value = "${aws_iam_instance_profile.instance_profile.id}"
}

output "ec2-instance-profile-role-DXCMSP-DevSci_id" {
  value = "${aws_iam_role.instance_profile.id}"
}
