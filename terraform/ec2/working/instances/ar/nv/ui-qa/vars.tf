variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "SSL_PASSWORD" {}

variable "aws_region" {
  default = "us-east-2"
}

variable "az" {
  default = "us-east-2a"
}

variable customer {
  default = "DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "RLIMS-QA"
}

variable "path_to_private_key" {
  default = "rlimskeypair-ui"
}

variable "path_to_public_key" {
  default = "rlimskeypair-ui.pub"
}

variable "instance_username" {
  default = "ec2-user"
}

# This is ui base AMI
variable "amis" {
  type = "map"

  default = {
    us-east-2 = ""
  }
}


variable "instance_type" {
  description = "Size of the instance"
  default     = "t2.large"
}

variable "exemplar_lims_server_ip" {
  default = "10.253.42.61"
}

variable "email_host_ip" {
  default = "15.163.124.148"
}

variable "primary_eni_id" {
  default = "eni-04d1acacc83772017"
}

variable "root_volume_size" {
  default = "250"
}

variable "exemplar_base" {
  default = "exemplarlims_v1060_b770.zip"
}

variable "devsci_abbvie" {
  default = "Base Delivery (LIMS 10.6.0).zip"
}

variable "devsci_guid" {
  default = ""
}

variable "discovery_guid" {
  default = ""
}

variable "discovery_abbvie" {
  default = "4 AbbVie - Primary.zip"
}

variable "discovery_extractor_name" {
  default = "abbvie-primary_extractor.jar"
}

variable "devsci_extractor_name" {
  default = "exemplar_10.6.0_extractor.jar"
}
