resource "aws_instance" "rlims-ui-instance" {
  ami           = "${lookup(var.amis, var.aws_region)}"
  instance_type = "${var.instance_type}"

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-ui.rendered}"

  provisioner "file" {
    source      = "/data/rlims/deployment"
    destination = "/tmp"
  }

  provisioner "file" {
    source      = "./scripts"
    destination = "/tmp"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-UI"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/ui-base.sh",
      "chmod +x /tmp/scripts/ui.sh",
      "sudo /tmp/scripts/ui-base.sh",
      "sudo /tmp/scripts/ui.sh ${var.exemplar_base} ${var.exemplar_lims_server_ip} ${var.email_host_ip}",
    ]
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.rlims-ui-instance.private_ip} >> ui-private_ips.txt"
  }

  connection {
    user        = "${var.instance_username}"
    private_key = "${file("${var.path_to_private_key}")}"
  }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "${var.root_volume_size}"
    delete_on_termination = true
  }

  network_interface {
    network_interface_id  = "${var.primary_eni_id}"
    delete_on_termination = false
    device_index          = 0
  }
}
