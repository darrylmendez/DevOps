variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
  default = "us-east-1"
}

variable "WIN_AMIS" {
  type = "map"

  default = {
    us-east-1 = "ami-050202fb72f001b47"
  }
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "INSTANCE_USERNAME" {
  default = "Terraform"
}

variable "INSTANCE_PASSWORD" {}

variable "security_group_ids" {
  description = "List of Security Group Id"
  type        = "list"

  default = [
    "sg-1fa8e954",
  ]
}
