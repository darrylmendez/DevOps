#!/usr/bin/env bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 03/28/2018
# Description	: Initial Revision
# Define Variables
LIBREOFFICE_RPM=LibreOffice_5.4.6_Linux_x86-64_rpm.tar.gz
EXEMPLAR_INSTALL_DIR=/opt/rlims
# Optional per Sapio
EXEMPLAR_OBFUSCATED_JAR=Exemplar961ObfuscatedJar
EXEMPLAR_BASE=exemplarlims_v961_b664
EXEMPLAR_LIMS_DELIVERY="AbbVie Delivery 6"
EXEMPLAR_BASE_LINUX_EXEC=exemplarlims_v961_b664
EXEMPLAR_LICENSE=exemplar.license
VELOX_SERVER=$EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/ 
PROGRAM_NAME=`basename "$0"`
TEMP_TOMCAT_PLUGIN_DIR=/home/ec2-user/tomcatplugins
TOMCAT_PLUGIN_DIR=$EXEMPLAR_INSTALL_DIR/tomcatplugins
ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
ABBVIE_INSTALL_DIR=$EXEMPLAR_INSTALL_DIR/abbvie/abbvieprod
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=$(DATE + "%y-%m-%d %T")
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }

#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip "$1" -d "$2"
}
#Install Libreoffice
install_libre_office {
  cd ~
  #tar –zxvf LibreOffice_5.3.7_Linux_x86-64_rpm.tar 
  tar -zxvf $LIBREOFFICE_RPM
  cd LibreOffice*/RPMS
  sudo yum –y install *.rpm

  #Confirm Libreoffice was installed correctly
  cd /opt
  if [! -d libreoffice5.3]; then
    exit 1
  fi

  sudo yum install -y cairo cups-libs libSM
}
plugin_install() {

}

exemplar_obfuscated() {
  logit INFO "Starting Exemplar Obfuscated Install" 
  logit INFO "Moving /tmp/$EXEMPLAR_OBFUSCATED_JAR.zip" to "$EXEMPLAR_INSTALL_DIR"  
  move_file /tmp/"$EXEMPLAR_OBFUSCATED_JAR.zip" "$EXEMPLAR_INSTALL_DIR" 
		|| logit ERROR "Move File Exemplar Obfuscated $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Starting Extract of Exemplar Obfuscated JAR $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR" 
  unzip_file "$EXEMPLAR_OBFUSCATED_JAR".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR 
		|| logit ERROR "Unzip File $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR Failed"	
  sudo chmod +x $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh 
  sudo chown -R $EXEMPLAR_INSTALL_DIR rlimsadmin:rlimsadmin *  
  nohup $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh &
}
exemplar_install() {
  logit INFO "Creating Exemplar Install Directory" 
  if [! -d "$EXEMPLAR_INSTALL_DIR"]; then
    mkdir $EXEMPLAR_INSTALL_DIR || logit ERROR "Exemplar Install Directory $EXEMPLAR_INSTALL_DIR Creation failed"
  fi
	
  logit INFO "Starting Upload of Base Exemplar Packages"
  logit INFO "Moving Exemplar Packages"  
  #full path may be needed
  move_file /tmp/"$EXEMPLAR_BASE.zip" "$EXEMPLAR_INSTALL_DIR" 
		|| logit ERROR "Move File Exemplar Base $EXEMPLAR_BASE.zip to $EXEMPLAR_INSTALL_DIR Failed"
  move_file /tmp/"$EXEMPLAR_LIMS_DELIVERY.zip" "$EXEMPLAR_INSTALL_DIR" 
		|| logit ERROR "Move File Exemplar LIMS Delivery $EXEMPLAR_LIMS_DELIVERY.zip to $EXEMPLAR_INSTALL_DIR Failed"
	
  #Install License 7
  logit INFO "Moving Exemplar License file to "$EXEMPLAR_INSTALL_DIR 
  move_file /tmp/"$EXEMPLAR_LICENSE" $EXEMPLAR_INSTALL_DIR 
		|| logit ERROR "Move File Exemplar License $EXEMPLAR_LICENSE to $EXEMPLAR_INSTALL_DIR Failed"
		
  cd $EXEMPLAR_INSTALL_DIR 
  
  #Unzip Exemplar Files
 
  unzip_file "$EXEMPLAR_BASE".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE 
		|| logit ERROR "Unzip File $EXEMPLAR_BASE to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE Failed"
  unzip_file "$EXEMPLAR_LIMS_DELIVERY".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_LIMS_DELIVERY 
		|| logit ERROR "Unzip File $EXEMPLAR_LIMS_DELIVERY.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_LIMS_DELIVERY Failed"
}

abbvie_delivery_extractor() {
  logit INFO "Installing AbbVie Delivery Extractor - AbbVie Customization" 
  if [! -d "$TEMP_TOMCAT_PLUGIN_DIR"]; then
    mkdir $TEMP_TOMCAT_PLUGIN_DIR || logit ERROR "Install Directory $TEMP_TOMCAT_PLUGIN_DIR Creation failed"
  fi
  cd $EXEMPLAR_INSTALL_DIR
  sudo su rlimsadmin
  
  if [! -d "$TOMCAT_PLUGIN_DIR"]; then
    mkdir $TOMCAT_PLUGIN_DIR || logit ERROR "Install Directory $TOMCAT_PLUGIN_DIR Creation failed"
  fi
  move_file /tmp/$ABBVIE_DELIVERY_EXTRACTOR $ABBVIE_INSTALL_DIR/
  java -D $1 -jar $ABBVIE_DELIVERY_EXTRACTOR
  

}
	
#Install Velox Server
#keep in packer
install_Velox_Server {
#keep in packer
  cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver || logit ERROR "cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver failed"
  sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC 
		|| logit ERROR "sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC failed"
	#it may not work in packer - will have to try it
  echo $EXEMPLAR_INSTALL_DIR | sudo ./$EXEMPLAR_BASE_LINUX_EXEC 
		|| logit ERROR "Installation of Exemplar LIMS Base Failed at $EXEMPLAR_INSTALL_DIR by $EXEMPLAR_BASE_LINUX_EXEC"

}

#modify the file using db property fil=-8
configure_velox_server_props {
   #pre-req - Oracle DB creation
  sudo cp $VELOX_SERVER/VeloxServer.properties $EXEMPLAR_INSTALL_DIR 
}

#Change ownership of directory
change_ownership {
  sudo chown -R "$1":"$2" $EXEMPLAR_INSTALL_DIR || logit ERROR "Change Ownership failed $1:$2 - $EXEMPLAR_INSTALL_DIR" 
}

#Verify Directory contents
verify_directory {
  if [! -d $1]; then
    exit 1
  fi 
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
	
  #keep it in packer
  logit INFO 'Starting Libreoffice Install'
  install_libre_office 
  ret_code = $?
  if [$ret_code -eq "0"] then
    logit INFO "Libreoffice install successful"
	exit 0
  else
	logit ERROR "Libreoffice install failed"
    exit 1
  fi
	
  logit INFO 'Starting Exemplar Install'
  exemplar_install
  ret_code = $?
  if [$ret_code -eq "0"] then
	logit INFO "Exemplar install successful"
	exit 0
  else
	logit ERROR "Exemplar install failed"
    exit 1
  fi
	
  logit INFO 'Starting Velox Server Install'
  install_Velox_Server
  ret_code = $?
  if [$ret_code -eq "0"] then
	logit INFO "Velox Server install successful"
	exit 0
  else
	logit ERROR "Velox Server install failed"
    exit 1
  fi
	
  logit INFO 'Starting Configure Velox Server Properties'
  #configure_velox_server_props 
  ret_code = $?
  if [$ret_code -eq "0"] then
	logit INFO "Configure Velox Server Properties successful"
	exit 0
  else
	logit ERROR "Configure Velox Server Properties failed"
    exit 1
  fi
  
  #Change ownership of Exemplar Install Directory to rlimsadmin user/group
  logit INFO 'Starting Change Ownership of '
  change_ownership rlimsadmin rlimsadmin
  ret_code = $?
  if [$ret_code -eq "0"] then
	logit INFO "Change Owner successful"
	exit 0
  else
	logit ERROR "Exemplar install failed"
    exit 1
  fi
  
  # 20 - Confirm portal client 'veloxClient.war' exist in EXEMPLAR_INSTALL/client 
  verify_directory $EXEMPLAR_INSTALL/client/veloxClient.war
  if [$ret_code -eq "0"] then
	logit INFO "$EXEMPLAR_INSTALL/client/veloxClient.war is extracted"
	exit 0
  else
	logit ERROR "$EXEMPLAR_INSTALL/client/veloxClient.war is not extracted"
    exit 1
  fi
  #32 Install Exemplar Obfustcated and DataMgmtServer.bsh
  exemplar_obfuscated()
  if [$ret_code -eq "0"] then
	logit INFO "Exemplar Obfuscated and DataMgmtServer.bsh installed"
	exit 0
  else
	logit ERROR "Exemplar Obfuscated and DataMgmtServer.bsh installation failed"
    exit 1
  fi
}