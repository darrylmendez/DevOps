#!/usr/bin/env bash
# Logging Function
{
set -e
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done
#Install utils
echo 'sudo yum install -y unzip wget zip vim libaio openssl'
sudo yum install -y unzip wget zip vim libaio openssl
echo 'Completed Utils Installation'
}
