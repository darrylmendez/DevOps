variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
  default = "us-east-2"
}

variable customer {
  default = "Abbvie-DevSci"
}

variable environment {
  description = "Name of the environment"
  default     = "DEVELOPMENT"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskeypair"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskeypair.pub"
}

variable "INSTANCE_USERNAME" {
  default = "ec2-user"
}

variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}

variable "AMIS" {
  type = "map"

  default = {
    us-east-2 = "ami-03291866"
  }
}

variable "rds_allocated_storage" {
  default = 100
}

variable "rds_oracle_engine" {
  default = "oracle-ee"
}

variable "rds_engine_version" {
  default = "12.1.0.2.v8"
}

variable "rds_iops" {
  default = 1000
}

variable "rds_instance_class" {
  default = "db.t2.micro"
}

variable "rds_identifier" {
  default = "rlimsdev"
}

variable "rds_db_name" {
  default = "RLIMSDEV"
}

variable "rds_user" {
  default = "rlimsadmin"
}

variable "rds_password" {
  default = "AbbVie2013"
}

variable "rds_storage_type" {
  default = "io1"
}

variable "rds_backup_retention" {
  default = 30
}

variable "app-db-instance-name" {
  default = "RLIMS-DEV-DB"
}

variable "db-instance-identifier-snapshot" {
  default = "rlimseedbinstancedev"
}

#variable "snapshot-identifier" {
#  default = "rds:rlimseedbinstancedev-2018-06-13-06-08"
#}
variable "snapshot-identifier" {
  default = "rlims-dev-snap-2018-06-21-06-09"
}

variable "oracle-parameter-name" {
  default = "oracle-db-parameters"
}

variable "skip_final_snapshot" {
  default = "true"
}

variable "guid" {
  default = "813a36c9-c0c7-4eb7-8261-2c12f03826cc#"
}
