#!/bin/bash
echo 'installing Oracle Instant Client'
echo 'sudo rpm -ivh oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm'
sudo rpm oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm

echo 'Installing Oracle Instant Client SQLPLUS'
echo 'sudo rpm -ivh oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.i386.rpm'
sudo rpm -ivh oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.i386.rpm

echo 'Setting ORACLE_HOME, LD_LIBRARY PATH'
export ORACLE_HOME='/usr/lib/oracle/12.2/client64' >> ~/.bash_profile
export PATH='$JAVA_HOME/bin:$PATH' >>~/.bash_profile
echo $PATH
source ~/.bash_profile
cat ~/.bash_profile

echo $2
echo $1
sqlplus -s rlimsadmin@$2 /nolog <<EOF
create user rlimsuser identified by 'AbbVie2013';
grant unlimited tablespace to rlimsuser;
grant create table to rlimsuser;
EOF
