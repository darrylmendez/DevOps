resource "aws_security_group" "rlims-sg" {
  vpc_id      = "${aws_vpc.main.id}"
  name        = "allow-ssh"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "rlims-sg"
  }
}

resource "aws_security_group" "allow-oracle" {
  vpc_id      = "${aws_vpc.main.id}"
  name        = "allow-oracle"
  description = "allow-oracle"

  ingress {
    from_port       = 1521
    to_port         = 1521
    protocol        = "tcp"
    security_groups = ["${aws_security_group.rlims-sg.id}"] # allowing access from our example instance
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  tags {
    Name = "rlims-allow-oracle"
  }
}
