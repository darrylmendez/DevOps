resource "aws_instance" "rlims-bls-instance" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"

  # the VPC subnet
  # move to private id 
  subnet_id = "subnet-95cc80fd"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.rlims-sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.rlimskeypair.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-rlims-bls.rendered}"

  provisioner "file" {
    source      = "/home/ubuntu/DownLoads"
    destination = "/tmp/DownLoads"
  }

  tags = {
    Name        = "${var.customer}-${var.environment}-BLS-Server"
    environment = "${var.environment}"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/*.sh",
      "sudo /tmp/scripts/utils.sh",
      "sudo /tmp/bls.sh ${var.guid}",
    ]
  }

  connection {
    user        = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
}
