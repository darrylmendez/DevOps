terraform {
  backend "s3" {
    bucket = "abbvie-rlims"
    key    = "terraform/demo"
    region = "us-east-1"
  }
}
