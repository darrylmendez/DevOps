resource "aws_db_subnet_group" "oracle-subnet" {
  name        = "oracle-subnet"
  description = "RDS subnet group"
  subnet_ids  = ["${aws_subnet.main-private-1.id}", "${aws_subnet.main-private-2.id}"]
}

#resource "aws_db_parameter_group" "oracle-parameters" {
#  name        = "oracle-db-parameters"
#  family      = "oracle-ee-12.1"
#  description = "Oracle DB parameter group"
#}

#not working - so using hardcode snapshot-identifier
#6/14/18
#data "aws_db_snapshot" "db_snapshot" {
#  most_recent            = true
#  db_instance_identifier = "${var.db-instance-identifier-snapshot}"
#}

resource "aws_db_instance" "oracle" {
  allocated_storage    = "${var.rds_allocated_storage}"
  engine               = "${var.rds_oracle_engine}"
  engine_version       = "${var.rds_engine_version}"
  iops                 = "${var.rds_iops}"
  instance_class       = "${var.rds_instance_class}"
  identifier           = "${var.rds_identifier}"
  name                 = "${var.rds_db_name}"
  username             = "${var.rds_user}"
  password             = "${var.rds_password}"
  db_subnet_group_name = "${aws_db_subnet_group.oracle-subnet.name}"

  #snapshot_identifier     = "${data.aws_db_snapshot.db_snapshot.id}"
  snapshot_identifier = "${var.snapshot-identifier}"

  #parameter_group_name    = "${aws_db_parameter_group.oracle-parameters.name}"
  parameter_group_name    = "${var.oracle-parameter-name}"
  multi_az                = "false"
  vpc_security_group_ids  = ["${aws_security_group.allow-oracle.id}"]
  storage_type            = "${var.rds_storage_type}"
  backup_retention_period = "${var.rds_backup_retention}"
  availability_zone       = "${aws_subnet.main-private-1.availability_zone}"
  skip_final_snapshot     = "${var.skip_final_snapshot}"

  tags {
    Name = "${var.app-db-instance-name}"
  }
}
