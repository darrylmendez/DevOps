# Internet VPC
resource "aws_vpc" "rlims-main-vpc" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags {
    Name = "rlims-main-vpc"
  }
}

# Subnets
resource "aws_subnet" "rlims-public-subnet-1" {
  vpc_id                  = "${aws_vpc.rlims-main-vpc.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-2a"

  tags {
    Name = "rlims-main-public-1"
  }
}

resource "aws_subnet" "rlims-public-subnet-2" {
  vpc_id                  = "${aws_vpc.rlims-main-vpc.id}"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-2b"

  tags {
    Name = "rlims-main-public-2"
  }
}


resource "aws_subnet" "rlims-private-subnet-1" {
  vpc_id                  = "${aws_vpc.rlims-main-vpc.id}"
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-2a"

  tags {
    Name = "rlims-private-subnet-1"
  }
}

resource "aws_subnet" "rlims-private-subnet-2" {
  vpc_id                  = "${aws_vpc.rlims-main-vpc.id}"
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-2b"

  tags {
    Name = "rlims-private-subnet-2"
  }
}



# Internet GW
resource "aws_internet_gateway" "rlims-gw" {
  vpc_id = "${aws_vpc.rlims-main-vpc.id}"

  tags {
    Name = "rlims-ig"
  }
}

# route tables
resource "aws_route_table" "rlims-public-1" {
  vpc_id = "${aws_vpc.rlims-main-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.main-gw.id}"
  }

  tags {
    Name = "rlims-public-1"
  }
}

# route associations public
resource "aws_route_table_association" "rlims-public-1-a" {
  subnet_id      = "${aws_subnet.rlims-public-subnet-1.id}"
  route_table_id = "${aws_route_table.main-public.id}"
  
}

resource "aws_route_table_association" "rlims-main-public-2-a" {
  subnet_id      = "${aws_subnet.rlims-public-subnet-1.id}"
  route_table_id = "${aws_route_table.rlims-public-1.id}"

 
}

