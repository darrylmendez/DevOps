variable "AWS_REGION" {
  default = "us-east-2"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskeypair"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskeypair.pub"
}
variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-db2919be"
    us-west-2 = "ami-06b94666"
  }
}

