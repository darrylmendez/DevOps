variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "us-east-2"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-03291866"
  }
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskey.pub"
}
variable "INSTANCE_USERNAME" {
  default = "ec2"
}
