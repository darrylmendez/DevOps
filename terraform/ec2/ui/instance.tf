resource "aws_key_pair" "rlimskey" {
  key_name = "rlimskey"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}

resource "aws_instance" "rlims_ui" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"
  provisioner "local-exec" {
     command = "echo ${aws_instance.rlims_ui.private_ip} >> private_ips.txt"
  }
  
  # the public SSH key
  key_name = "${aws_key_pair.rlimskey.key_name}"

  provisioner "file" {
    source = "script.sh"
    destination = "/tmp/script.sh"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }
  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
  #Use template approach for dynamic lookup of SG and VPC
  
  vpc_security_group_ids = ["${var.AWS_SECURITY_GROUP_ALLOW_SSH_ID}"]

  
  # the VPC subnet
  subnet_id = "${var.AWS_SUBNET_MAIN_PUBLIC_1_ID}"

}

output "ip" {
    value = "${aws_instance.rlims_ui.private_ip}"
}


