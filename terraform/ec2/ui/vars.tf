variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "us-east-2"
}
variable "AMIS" {
  type = "map"
  default = {
    us-east-2 = "ami-03291866"
  }
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "rlimskey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "rlimskey.pub"
}
variable "INSTANCE_USERNAME" {
  default = "ec2"
}

#Make this dynamic lookup for a rainy day
variable "AWS_SUBNET_MAIN_PUBLIC_1_ID" {
  default = "subnet-1fcea177"
}

variable "AWS_SECURITY_GROUP_ALLOW_SSH_ID" {}
  