#!/bin/bash
export LIBREOFFICE_RPM=LibreOffice_5.4.6_Linux_x86-64_rpm.tar.gz
export JDK=jdk-8u162-linux-x64.rpm
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
#Handled by DXC??? 
upgrade() {
  logit INFO 'Starting Package Update'
  sudo yum update -y
  sudo yum upgrade -y
  logit INFO 'Package Successfully Updated'
}

install_utils() {
 #Perform Update
  logit INFO 'Starting Utils Install'
  sudo yum install -y unzip wget zip vim libaio openssl
  logit INFO 'Completed Utils Installation'
}
install_libre_office() {
  cd ~
  logit INFO 'Starting Libreoffice Install'
  tar -zxvf $LIBREOFFICE_RPM
  cd LibreOffice*/RPMS
  sudo yum –y install *.rpm
  sudo yum install -y cairo cups-libs libSM
  logit INFO 'Libre Office Successfully Installed'
}
add_rlims_user() {
  logit INFO 'Starting rlimsadmin user and group creation'
  sudo groupadd rlimsadmin
  sudo useradd -M -s /bin/nologin -g rlimsadmin -d /opt/rlims rlimsadmin
  logit INFO 'rlimadmin user and group created'
}
install_jdk() {
  logit INFO 'Moving rpm to opt'
  sudo mv /tmp/base/$JDK /opt/
  cd /opt/
  logit INFO 'Installing JDK'
  sudo rpm -ivh $JDK
  echo "export JAVA_HOME=/opt/jdk1.8.0_162/bin" >> ~/.bash_profile
  echo "export JRE_HOME=/opt/jdk1.8.0_162/jre/bin" >> ~/.bash_profile
  echo "export PATH=$JAVA_HOME:$JRE_HOME:$PATH" >> ~/.bash_profile
  source ~/.bash_profile
 
}
cleanup() {
  cd /home/ec2-user
  sudo rm $LIBREOFFICE_RPM
}
{
  logit INFO '.....Starting BLS Base Installation'
  logit INFO '...................................'
  logit INFO 'Starting Package update/upgrade'
  logit INFO '...................................'
  upgrade
  logit INFO '...................................' 
  logit INFO 'Completed Package update/upgrade'
  logit INFO '...................................' 
  logit INFO 'Starting Utils Install'
  install_utils
  logit INFO 'Completed Utils Install'
  logit INFO 'Starting Libreoffice Install'
  install_libre_office 
  logit INFO 'Completed Libreoffice Install'
  logit INFO 'Creating User/Group'
  add_rlims_user
  logit INFO 'Completed User/Group creation'
  logit INFO 'Starting JDK Install'
  install_jdk
  logit INFO 'Completed JDK Install'
  logit INFO '.....BLS Base installation and configuration complete'
  echo '.....BLS Base installation and configuration complete'
}
