#!/bin/bash
# Packer RLIMS UI Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision

#EXEMPLAR Base Installation zip
export EXEMPLAR_BASE=exemplarlims_v961_b664
#AbbVie Customization JAR File
export ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/common
export EXEMPLAR_WS_ZIP="Sapio Web Service Delivery (LIMS 9.6.1)".zip
export EXEMPLAR_WS_EXTRACTOR=exemplar_9.6.1_extractor.jar
export GUID=$1
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
deploy_lims_jars() {
  unzip_file $STAGE_DIR/"$EXEMPLAR_BASE.zip" $STAGE_DIR
  sudo cp $STAGE_DIR/veloxClient/veloxClient.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/veloxclient/veloxClient.war to $TOMCAT_APP_INSTALL"
  sudo cp $STAGE_DIR/velox_portal/velox_portal.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/velox_portal/velox_portal.war to $TOMCAT_APP_INSTALL"
}
abbvie_delivery_extractor() {
  sudo java -Dappguid=$GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR
}
deploy_ws() {
  sudo java -D $GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$EXEMPLAR_WS_EXTRACTOR -donotprompt
}
{
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server App Install'
  logit INFO '-------------------------------------------'

  logit INFO '-------------------------------------------'
  logit INFO 'Deploy LIMS JARS'
  logit INFO '-------------------------------------------'
  deploy_lims_jars
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO "Deploy LIMS JARS successful"
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO "Deploy LIMS JARS Failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  

  if [ $ec = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Sucessfully Completed UI Server APP Install'
    logit INFO '-------------------------------------------'
  fi
}
