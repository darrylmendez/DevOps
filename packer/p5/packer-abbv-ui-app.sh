#!/bin/bash
# RLIMS UI/Tomcat Application Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
export EXEMPLAR_BASE=exemplarlims_v961_b664
export TOMCAT_APP_INSTALL=/opt/tomcat/webapps
export STAGE_DIR=/tmp/common
export GUID=$1
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
deploy_lims_jars() {
  #unzip -p $STAGE_DIR/"$EXEMPLAR_BASE.zip" /veloxclient/veloxClient.war || logit ERROR "Error extracting /veloxclient/veloxClient.war from" $STAGE_DIR/"$EXEMPLAR_BASE.zip" 
  #unzip -p $STAGE_DIR/"$EXEMPLAR_BASE.zip" /velox_portal/velox_portal.war || logit ERROR "Error extracting /velox_portal/velox_portal.war from" $STAGE_DIR/"$EXEMPLAR_BASE.zip"
  unzip_file $STAGE_DIR/"$EXEMPLAR_BASE.zip" $STAGE_DIR
  sudo cp $STAGE_DIR/veloxClient/veloxClient.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/veloxclient/veloxClient.war to $TOMCAT_APP_INSTALL"
  sudo cp $STAGE_DIR/velox_portal/velox_portal.war $TOMCAT_APP_INSTALL || logit ERROR "Error copying $STAGE_DIR/velox_portal/velox_portal.war to $TOMCAT_APP_INSTALL"
}
abbvie_delivery_extractor() {
  #move_file $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR $ABBVIE_INSTALL_DIR/ || logit ERROR "Error moving file $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR to "
  #$ java -Dappguid=<APP_GUID> -Dtomcatpath=/usr/local/<Exemplar_Install_Directory>/tomcatplugins -jar abbviedelivery_extractor.jar
  sudo java -Dappguid=$GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$ABBVIE_DELIVERY_EXTRACTOR
}
deploy_ws() {
  #unzip_file $STAGE_DIR/$EXEMPLAR_WS_ZIP $STAGE_DIR
  sudo java -D $GUID -Dtomcatpath=$TOMCAT_APP_INSTALL -jar $STAGE_DIR/$EXEMPLAR_WS_EXTRACTOR -donotprompt
}
{
  ec=0
  logit INFO '-------------------------------------------'
  logit INFO 'Starting UI Server App Install'
  logit INFO '-------------------------------------------'

  logit INFO '-------------------------------------------'
  logit INFO 'Deploy LIMS JARS'
  logit INFO '-------------------------------------------'
  deploy_lims_jars
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-------------------------------------------'
    logit INFO "Deploy LIMS JARS successful"
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO "Deploy LIMS JARS Failed"
    logit INFO '-------------------------------------------'
	exit 1
  fi
  
  logit INFO '-------------------------------------------'
  logit INFO 'Starting abbvie_delivery_extractor '
  logit INFO '-------------------------------------------'
  abbvie_delivery_extractor
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'AbbVie Delivery Extractor Completed'
    logit INFO '-------------------------------------------'
  else
  	ec=1
    logit INFO '-------------------------------------------'
    logit INFO 'AbbVie Delivery Extractor Failed'
    logit INFO '-------------------------------------------'
	exit 1
  fi
  
  logit INFO '-------------------------------------------'
  logit INFO 'Exemplar Web Service Deployment'
  logit INFO '-------------------------------------------'
  deploy_ws
  rc=$?
  if [ $rc = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Exemplar Web Service Deployment Successful'
    logit INFO '-------------------------------------------'
  else
    logit INFO '-------------------------------------------'
    logit INFO 'Exemplar Web Service Deployment Failed'
    logit INFO '-------------------------------------------'
	ec=1
	exit 1
  fi
  if [ $ec = "0" ]; then  
    logit INFO '-------------------------------------------'
    logit INFO 'Sucessfully Completed UI Server APP Install'
    logit INFO '-------------------------------------------'
  fi
}
