#!/usr/bin/bash
# RLIMS BLS Server Installation Script
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 03/28/2018
# Description	: Initial Revision
# Define Variables
EXEMPLAR_INSTALL_DIR=/opt/rlims
# Optional per Sapio
EXEMPLAR_OBFUSCATED_JAR=Exemplar961ObfuscatedJar
EXEMPLAR_BASE=exemplarlims_v961_b664
EXEMPLAR_LIMS_DELIVERY="AbbVie Delivery 6"
EXEMPLAR_BASE_LINUX_EXEC=exemplarlims_v961_b664_linux
EXEMPLAR_LICENSE=exemplar.license
VELOX_SERVER=$EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/ 
PROGRAM_NAME=`basename "$0"`
ABBVIE_DELIVERY_EXTRACTOR=abbviedelivery_extractor.jar
ABBVIE_INSTALL_DIR=$EXEMPLAR_INSTALL_DIR/abbvie/abbvieprod
# Logging Function
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date` 
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }

#move files
move_file() {
  sudo mv "$1" "$2"
}
#unzip files
unzip_file() {
  sudo unzip "$1" -d "$2"
}

exemplar_obfuscated() {
  logit INFO "Starting Exemplar Obfuscated Install" 
  logit INFO "Killing DataMgmtServer.bsh Process id "   
  ps aux | grep [D]ataMgmtServer.bsh | awk '{print $2}' | kill -9
  logit INFO "Moving /tmp/BLS/$EXEMPLAR_OBFUSCATED_JAR.zip" to "$EXEMPLAR_INSTALL_DIR"  
  move_file /tmp/BLS/"$EXEMPLAR_OBFUSCATED_JAR.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar Obfuscated $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Starting Extract of Exemplar Obfuscated JAR $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR" 
  unzip_file "$EXEMPLAR_OBFUSCATED_JAR".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR || logit ERROR "Unzip File $EXEMPLAR_OBFUSCATED_JAR.zip to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_OBFUSCATED_JAR Failed"	
  sudo chmod +x $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh 
  sudo chown -R $EXEMPLAR_INSTALL_DIR rlimsadmin:rlimsadmin *  
  nohup $EXEMPLAR_INSTALL_DIR/DataMgmtServer.bsh &
}
exemplar_install() {
  logit INFO "Creating Exemplar Install Directory" 
  #if [! -d "$EXEMPLAR_INSTALL_DIR"]; then
    sudo mkdir $EXEMPLAR_INSTALL_DIR || logit ERROR "Exemplar Install Directory $EXEMPLAR_INSTALL_DIR Creation failed"
  #fi
	
  logit INFO "Starting Upload of Base Exemplar Packages"
  logit INFO "Moving Exemplar Packages"  
  #full path may be needed
  move_file /tmp/BLS/"$EXEMPLAR_BASE.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar Base $EXEMPLAR_BASE.zip to $EXEMPLAR_INSTALL_DIR Failed"
  move_file /tmp/BLS/"$EXEMPLAR_LIMS_DELIVERY.zip" "$EXEMPLAR_INSTALL_DIR" || logit ERROR "Move File Exemplar LIMS Delivery $EXEMPLAR_LIMS_DELIVERY.zip to $EXEMPLAR_INSTALL_DIR Failed"
	
  #Install License 7
  logit INFO "Moving Exemplar License file to "$EXEMPLAR_INSTALL_DIR 
  move_file /tmp/BLS/"$EXEMPLAR_LICENSE" $EXEMPLAR_INSTALL_DIR || logit ERROR "Move File Exemplar License $EXEMPLAR_LICENSE to $EXEMPLAR_INSTALL_DIR Failed"
  logit INFO "Moved Exemplar License file to "$EXEMPLAR_INSTALL_DIR		
  logit INFO "Changing Directory "$EXEMPLAR_INSTALL_DIR 
  cd $EXEMPLAR_INSTALL_DIR 
  logit INFO "Changed Directory to " `pwd` 
  logit INFO "Unzip Exemplar Files"
  logit INFO "Unzip File " "$EXEMPLAR_BASE".zip " To " $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE 
  unzip_file "$EXEMPLAR_BASE".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE || logit ERROR "Unzip File $EXEMPLAR_BASE to $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE Failed"
  logit INFO "Unzipped File " "$EXEMPLAR_BASE".zip " To " $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE 
  logit INFO "Unzip file ""$EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_LIMS_DELIVERY 
  unzip_file "$EXEMPLAR_LIMS_DELIVERY".zip $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_LIMS_DELIVERY 
  logit INFO "Unzipped file ""$EXEMPLAR_LIMS_DELIVERY".zip " To " $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_LIMS_DELIVERY 

  }

#Install Velox Server
#keep in packer
install_Velox_Server() {
#keep in packer
  cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver || logit ERROR "cd $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver failed"
  sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC 	|| logit ERROR "sudo chmod +x $EXEMPLAR_INSTALL_DIR/$EXEMPLAR_BASE/veloxserver/$EXEMPLAR_BASE_LINUX_EXEC failed"
	#it may not work in packer - will have to try it
  echo $EXEMPLAR_INSTALL_DIR | sudo ./$EXEMPLAR_BASE_LINUX_EXEC || logit ERROR "Installation of Exemplar LIMS Base Failed at $EXEMPLAR_INSTALL_DIR by $EXEMPLAR_BASE_LINUX_EXEC"
}

#Change ownership of directory
change_ownership() {
  logit INFO "Change Ownership " "$1":"$2" $EXEMPLAR_INSTALL_DIR
  sudo chown -R "$1":"$2" $EXEMPLAR_INSTALL_DIR || logit ERROR "Change Ownership failed $1:$2 - $EXEMPLAR_INSTALL_DIR" 
  logit INFO "Change Ownership Successful " "$1":"$2" $EXEMPLAR_INSTALL_DIR
}
# *******************************************************************
# ##### MAIN ########################################################
# *******************************************************************
#./setvars.sh
{
  set -e
  logit INFO 'Starting Exemplar Install'
  exemplar_install
  #ret_code = $?
  #if [$? -eq "0"]; then
	logit INFO "Exemplar install successful"
  #	exit 0
  #else
	#logit ERROR "Exemplar install failed"
   # exit 1
  #fi
	
  logit INFO 'Starting Velox Server Install'
  install_Velox_Server
  #ret_code = $?
  #if [$? -eq "0"]; then
	#logit INFO "Velox Server install successful"
	#exit 0
 # else
	#logit ERROR "Velox Server install failed"
   # exit 1
  #fi
	
 
  #Change ownership of Exemplar Install Directory to rlimsadmin user/group
  logit INFO 'Starting Change Ownership of '
  change_ownership rlimsadmin rlimsadmin
  #ret_code = $?
  #if [$? -eq "0"]; then
	logit INFO "Change Owner successful"
	#exit 0
  #else
	#logit ERROR "Exemplar install failed"
  #  exit 1
 ## fi
  
  # 20 - Confirm portal client 'veloxClient.war' exist in EXEMPLAR_INSTALL/client 
 # verify_directory $EXEMPLAR_INSTALL/client/veloxClient.war
 # if [$? -eq "0"]; then
	#logit INFO "$EXEMPLAR_INSTALL/client/veloxClient.war is extracted"
#	exit 0
 # else
	#logit ERROR "$EXEMPLAR_INSTALL/client/veloxClient.war is not extracted"
 #   exit 1
 # fi
  #32 Install Exemplar Obfustcated and DataMgmtServer.bsh
  exemplar_obfuscated
  #if [$? -eq "0"]; then
	logit INFO "Exemplar Obfuscated and DataMgmtServer.bsh installed"
#	exit 0
 # else
	#logit ERROR "Exemplar Obfuscated and DataMgmtServer.bsh installation failed"
 #   exit 1
 # fi
}