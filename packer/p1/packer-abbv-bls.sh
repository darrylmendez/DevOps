#!/bin/bash
LIBREOFFICE_RPM=LibreOffice_5.4.6_Linux_x86-64_rpm.tar.gz
logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
#Handled by DXC??? 
upgrade() {
  logit INFO 'Starting Package Update'
  sudo yum update -y
  sudo yum upgrade -y
  logit INFO 'Package Successfully Updated'
}

install_utils() {
 #Perform Update
  logit INFO 'Starting Utils Install'
  echo 'sudo yum install -y unzip wget zip vim libaio openssl'
  sudo yum install -y unzip wget zip vim libaio openssl
  echo 'Completed Utils Installation'
  logit INFO 'Completed Utils Installation'
}
install_libre_office() {
  cd ~
  logit INFO 'Starting Libreoffice Install'
  tar -zxvf $LIBREOFFICE_RPM
  cd LibreOffice*/RPMS
  sudo yum –y install *.rpm
  sudo yum install -y cairo cups-libs libSM
  logit INFO 'Libre Office Successfully Installed'
}
add_rlims_user() {
  logit INFO 'Starting rlimsadmin user and group creation'
  echo 'Add rlimsadmin user and group'
  sudo groupadd rlimsadmin
  sudo useradd -M -s /bin/nologin -g rlimsadmin -d /opt/rlims rlimsadmin
  logit INFO 'rlimadmin user and group created'
}
{
  logit INFO '.....Starting BLS Base Installation'
  echo '.....Starting BLS Base Installation'
  logit INFO 'Starting Package update/upgrade'
  upgrade
  logit INFO 'Starting Utils Install'
  install_utils
  logit INFO 'Starting Libreoffice Install'
  install_libre_office 
  logit INFO 'Starting Creating Tomcat User/Group'
  add_rlims_user
  logit INFO '.....BLS Base installation and configuration complete'
  echo '.....BLS Base installation and configuration complete'
}
