#!/bin/bash
# RLIMS UI/Tomcat Server Base Installation
# Author		: Darryl Mendez (mendedx) 
# Email			: darryl.mendez@abbvie.com
# Date			: 07/31/2018
# Description	: Initial Revision
export TOMCAT_ZIP=apache-tomcat-9.0.10.tar.gz
export STAGE_DIR=/tmp/base
export TOMCAT_DIR=/opt/tomcat
export JDK=jdk-8u162-linux-x64.rpm

logit() {
  local LOG_LEVEL=$1
  shift 
  MSG=$@ 
  TIMESTAMP=`date`
  logger -s "${TIMESTAMP} ${HOST} ${PROGRAM_NAME} [$PID]: ${LOG_LEVEL} $MSG}"
 }
move_file() {
  sudo mv "$1" "$2" || logit ERROR "Error moving $1 to $2"
}
unzip_file() {
  sudo unzip "$1" -d "$2" || logit ERROR "Error unzipping $1 to $2"
}
create_group() {
  sudo groupadd $1 || logit ERROR "Error creating group $1"
}
create_user() {
  sudo useradd -M -s /bin/nologin -g $1 -d $2 $3 || logit ERROR "Error creating user $1 $2 $3"
}
install_utils() {
  sudo yum install -y unzip wget zip vim libaio openssl || logit ERROR "Error installing Utilities - unzip wget zip vim libaio openssl"
}
install_jdk() {
  logit INFO 'Moving rpm to opt'
  sudo mv $STAGE_DIR/$JDK /opt/
  cd /opt/
  logit INFO 'Installing JDK'
  sudo rpm -ivh $JDK
  echo "export JAVA_HOME=/opt/jdk1.8.0_162/bin" >> ~/.bash_profile
  echo "export JRE_HOME=/opt/jdk1.8.0_162/jre/bin" >> ~/.bash_profile
  echo "export PATH=$JAVA_HOME:$JRE_HOME:$PATH" >> ~/.bash_profile
  source ~/.bash_profile
}
install_tomcat() {
  logit INFO "Creating tomcat Directory : $TOMCAT_DIR"
  sudo mkdir $TOMCAT_DIR
  #sudo mkdir /opt/tomcat
  logit INFO "Extracting $TOMCAT_ZIP from $STAGE_DIR to $TOMCAT_DIR"
  #unzip_file $STAGE_DIR/$TOMCAT_ZIP || logit ERROR "Extracting $TOMCAT_ZIP from $STAGE_DIR to $TOMCAT_DIR"

  sudo tar xvzf $STAGE_DIR/$TOMCAT_ZIP -C $TOMCAT_DIR --strip-components=1
  
  sudo chown -Rf tomcat:tomcat $TOMCAT_DIR
  
  sudo chmod -R g+r /opt/tomcat/conf
  sudo chmod g+x /opt/tomcat/conf
  sudo mv $STAGE_DIR/tomcat.service /etc/systemd/system/
  sudo chmod 644 /etc/systemd/system/tomcat.service
  sudo chown root:root /etc/systemd/system/tomcat.service
  sudo systemctl daemon-reload
  sudo systemctl enable tomcat
 
}
{
  ec=0
  logit INFO '-----------------------------------------------'
  logit INFO 'Starting UI Server Base Install'
  logit INFO '-----------------------------------------------'
   
  logit INFO '-----------------------------------------------'
  logit INFO 'Installing JDK '
  logit INFO '-----------------------------------------------'
  install_jdk
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'JDK Sucessfully Installed'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'JDK Install Failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi

  logit INFO '-----------------------------------------------'
  logit INFO 'Installing unzip wget zip vim libaio openssl'
  logit INFO '-----------------------------------------------'
  install_utils
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'UTILS Installed'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'UTILS Installation failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi 
  logit INFO '-----------------------------------------------'
  logit INFO 'Creating tomcat group'
  logit INFO '-----------------------------------------------'
  create_group tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat group created'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat group creatopm failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  logit INFO '-----------------------------------------------'
  logit INFO 'Creating tomcat user'
  logit INFO '-----------------------------------------------'
  create_user tomcat $TOMCAT_DIR tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'Created tomcat user'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat user failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  
  logit INFO '-----------------------------------------------'
  logit INFO 'Installing tomcat '
  logit INFO '-----------------------------------------------'
  install_tomcat
  rc=$?
  if [ $rc = "0" ]; then
    logit INFO '-----------------------------------------------'
    logit INFO 'Installed tomcat'
    logit INFO '-----------------------------------------------'
  else
    ec=1
    logit INFO '-----------------------------------------------'
    logit INFO 'tomcat installation failed'
    logit INFO '-----------------------------------------------'
    exit 1
  fi
  if [ $ec = "0" ]; then  
    logit INFO '-----------------------------------------------'
    logit INFO 'Completed UI Server Base Install'
    logit INFO '-----------------------------------------------'
  else
    logit INFO '-----------------------------------------------'
    logit INFO 'Completed UI Server Base Install with errors'
    logit INFO '-----------------------------------------------'
  fi  
}
