#!/bin/bash

#AbbVie RedHat Enterprise Linux (RHEL) Base AMI Creation Script
#Author: Justin Doss (dossjj) justin.doss@abbvie.com
#Update: Thomas Baik (baikts) thomas.baik@abbvie.com
#Date: 01/22/2018

# Create a new user and group that will run the Tomcat service.
# For security purposes, Tomcat should be run as an unprivileged user (i.e. not root).
echo 'Add tomcat user and group'
sudo groupadd tomcat
sudo useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat

# Download and install Tomcat
echo 'Download and install tomcat'
cd /opt
sudo mkdir tomcat
sudo wget http://apache.mirrors.pair.com/tomcat/tomcat-8/v8.5.28/bin/apache-tomcat-8.5.28.tar.gz
sudo tar xvzf apache-tomcat-8.5.28.tar.gz -C /opt/tomcat --strip-components=1

echo 'Change ownership and privileges of tomcat installation'
# Change ownership of the tomcat files to the user tomcat
sudo chown -Rf tomcat:tomcat /opt/tomcat

# Give the tomcat group read access to the conf directory and all of its contents and
# execute access to the directory itself
sudo chmod -R g+r /opt/tomcat/conf
sudo chmod g+x /opt/tomcat/conf

echo 'Configure chkconfig to have tomcat start at boot time'
# Use the chkconfig utility to have Tomcat start at boot time. Using chkconfig: 234 20 80.
# 234 are the run levels and 20 and 80 are the stop and start priorities, respectively.
sudo mv /home/ec2-user/tomcat.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/tomcat.service
sudo chown root:root /etc/systemd/system/tomcat.service
sudo systemctl daemon-reload
sudo systemctl enable tomcat

echo '.....Tomcat installation and configuration complete'